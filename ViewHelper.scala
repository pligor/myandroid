package com.pligor.myandroid

import android.view.View
import android.view.View.OnClickListener
import com.pligor.myandroid.AndroidHelper._
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ViewHelper {
  def permanentlyDisableView(view: View,
                             backgroundDrawableId: Int,
                             messageId: Int)(implicit context: Context): Unit = {
    view.setBackgroundResource(backgroundDrawableId)

    view.setClickable(true)

    view.setOnClickListener(new OnClickListener {
      def onClick(view: View): Unit = {
        showToast(messageId)
      }
    });
  }
}
