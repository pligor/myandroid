package com.pligor.myandroid

import android.content.{Context, ServiceConnection, Intent}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
class SafeStartServiceTask[T](val serviceConnection: ServiceConnection,
                                      val itsClass: Class[T],
                                      val serviceObject: MyServiceObject)
                                     (implicit val context: Context)
  extends JavaAsyncTask[Long, AnyRef, Boolean] {
  protected def doInBackground(param: Long): Boolean = {
    try {
      while (serviceObject.isStarted) {
        Thread.sleep(param)
      }

      true
    } catch {
      case e: InterruptedException => false
    }
  }

  override def onPostExecute(result: Boolean): Unit = {
    super.onPostExecute(result)
    if (result) {
      //start service
      {
        val intent = new Intent(context, itsClass)

        val name = context.startService(intent)

        if (Option(name).isDefined) {
          log log "service is already started with name: " + name
        } else {
          log log "service is about to start"
        }
      }

      //bind to service
      {
        val intent = new Intent(context, itsClass)
        //DO NOT USE AUTO CREATE BECAUSE IT WILL NOT BE A STICKY SERVICE
        val bounded = context.bindService(intent, serviceConnection, {
          val BIND_NO_AUTO_CREATE = 0
          BIND_NO_AUTO_CREATE
        })

        assert(bounded, "service could not be bounded")
      }
    } else {
      log log "no need to do anything, service was not started because of INFINITE while loop ??"
    }
  }
}
