package com.pligor.myandroid

import android.app.ProgressDialog
import android.content.{Context, DialogInterface}
import scala.concurrent.ExecutionContext

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait JsonRequestActivity extends ProgressDialogSActivity
with JsonRequestTrait {

  //abstract

  //concrete

  /**
   * @param context THIS IMPLICIT CONTEXT IS VERY CRUCIAL, SCALAJ THROWS EXCEPTION OTHERWISE
   */
  def doJsonRequest(progressDialogTitle: String,
                    progressDialogMessage: String,
                    isProgressDialogShown: Boolean,
                    jsonRequestParam: JsonRequestParam)
                   (op: Option[JsonResponseResult] => Unit)(implicit context: Context, executionContext: ExecutionContext): () => Boolean = {

    val progressDialogOption = if (isProgressDialogShown) {
      val progDialog = registerProgressDialog({
        val indeterminate = true

        val cancelable = true

        ProgressDialog.show(
          context,
          progressDialogTitle,
          progressDialogMessage,
          indeterminate,
          cancelable
        )
      })

      Some(progDialog)
    } else {
      None
    }

    val cancellor = super.doJsonRequest(jsonRequestParam)({
      jsonResponseResultOption =>
        runOnUiThread({
          progressDialogOption.map(_.dismiss())

          op(jsonResponseResultOption)
        })
    })

    progressDialogOption.map(_.setOnCancelListener(new DialogInterface.OnCancelListener {
      def onCancel(dialog: DialogInterface): Unit = {
        cancellor.apply()
      }
    }))

    cancellor
  }

}
