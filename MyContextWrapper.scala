package com.pligor.myandroid

import android.content.{Context, ContextWrapper, BroadcastReceiver}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyContextWrapper extends ContextWrapper {
  protected implicit val context: Context = this

  protected implicit lazy val resources = getResources

  def safeUnregisterReceiver(receiver: BroadcastReceiver): Unit = {
    try {
      unregisterReceiver(receiver)
    }
    catch {
      case e: IllegalArgumentException => {
        //do nothing for the time being
      }
    }
  }
}
