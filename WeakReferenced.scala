package com.pligor.myandroid

import android.app.Activity
import scala.ref.WeakReference

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * http://theopentutorials.com/tutorials/android/dialog/android-badtokenexception-unable-to-add-window-is-your-activity-running/
 */
trait WeakReferenced {
  this: Activity =>

  private val activityWeakRef = WeakReference(this)

  protected def workWithActivity[T](op: Activity => T, activityNotAvail: => T): T = {
    val activityOption = activityWeakRef.get
    if (activityOption.isDefined && !activityOption.get.isFinishing) {
      op(activityOption.get)
    } else {
      activityNotAvail
    }
  }
}
