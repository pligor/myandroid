package com.pligor.myandroid

import android.net.Uri
import android.support.v4.content.CursorLoader
import android.content.Context
import android.database.Cursor
import java.io.File

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object UriHelper {
  def eraseUri(uri: Uri) = {
    //below line does not work, find out why
    //getContentResolver.delete(uri, null, null);

    //this line does not work either
    //val uriDeleted = new File(getRealPathFromURI(uri)).delete();

    /*log log "uri path is: " + uri.getPath;
    log log "real uri path is: " + getRealPathFromURI(uri);*/

    val uriDeleted = new File(uri.getPath).delete()

    log log (if (uriDeleted) "uri deleted" else "uri was not deleted damn it")

    //but actually the picture still remains because the uri we are getting is the temporary file,
    //this file is also getting written inside DCIM folder!
  }

  /**
   * http://stackoverflow.com/questions/3401579/get-filename-and-path-from-uri-from-mediastore
   * so far it is returning null!
   */
  def getRealPathFromURI(contentUri: Uri)(implicit context: Context): String = {
    val data = android.provider.MediaStore.MediaColumns.DATA;
    val projection = Array[String](data);
    val cursorLoader = new CursorLoader(context, contentUri, projection, null, null, null);
    val cursor: Cursor = cursorLoader.loadInBackground();
    val columnIndex: Int = cursor.getColumnIndexOrThrow(data);
    cursor.moveToFirst();
    cursor.getString(columnIndex);
  }

  /**
   * http://stackoverflow.com/questions/18323292/java-net-urlencoder-encode-encodes-space-as-but-i-need-20
   *
   * sample: Uri.parse(Fcarrier.SERVER_HOSTNAME + "/carrier/image/download/").buildUpon().appendPath(carrierName).toString
   */
  def uriParse(baseUri: String, unEncodedPaths: String*) = {
    val builder = Uri.parse(baseUri).buildUpon()

    unEncodedPaths.foreach {
      unEncodedPath =>
        builder.appendPath(unEncodedPath)
    }

    builder.toString
  }

}
