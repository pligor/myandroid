package com.pligor.myandroid

import android.widget.TextView
import android.view.KeyEvent

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait OnEnterPressedListener extends TextView.OnEditorActionListener {
  def currentIME: Int; // = EditorInfo.IME_ACTION_DONE

  def isEnterPressed(actionId: Int, keyEvent: KeyEvent): Boolean = {
    actionId == currentIME //&& keyEvent.getAction == KeyEvent.ACTION_UP
  }
}
