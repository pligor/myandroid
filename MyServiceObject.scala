package com.pligor.myandroid

import android.content.{ServiceConnection, Context, Intent}
import android.os.{Message, Handler, Messenger, IBinder}
import android.app.Service

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object MyServiceObject {
  def startAndBind[T](itsClass: Class[T], serviceConnection: ServiceConnection)
                     (implicit context: Context): Boolean = {
    //start service
    {
      val intent = new Intent(context, itsClass)

      val name = context.startService(intent)

      if (Option(name).isDefined) {
        log log s"service is already started with name: $name"
      } else {
        log log "service is about to start"
      }
    }

    bind(itsClass, serviceConnection)
  }

  def bind[T](itsClass: Class[T], serviceConnection: ServiceConnection)
             (implicit context: Context): Boolean = {
    val intent = new Intent(context, itsClass)

    context.bindService(intent, serviceConnection, {
      val BIND_NO_AUTO_CREATE = 0
      BIND_NO_AUTO_CREATE
    })
  }
}

trait MyServiceObject {
  //abstract

  //concrete

  var isStarted = false
}

trait MyStickyService extends Service {
  //abstract

  protected def myServiceObject: MyServiceObject

  protected def onMessage(msg: Message): Unit

  //concrete

  implicit protected val androidContext: Context = this

  def onBind(intent: Intent): IBinder = {
    myServiceObject.isStarted = true

    new Messenger(incomingHandler).getBinder
  }

  override def onStartCommand(intent: Intent, flags: Int, startId: Int): Int = {
    super.onStartCommand(intent, flags, startId)

    log log "sticky service is started"

    Service.START_STICKY //to remain and be exploited
  }

  override def onDestroy(): Unit = {
    super.onDestroy()

    log log "sticky service is destroyed"

    myServiceObject.isStarted = false
  }

  private object incomingHandler extends Handler {
    override def handleMessage(msg: Message): Unit = {
      super.handleMessage(msg)

      onMessage(msg)
    }
  }

}
