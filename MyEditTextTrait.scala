package com.pligor.myandroid

import android.widget.EditText

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyEditTextTrait {

  class MyEditText(editTextView: EditText) {
    def getTrimmed = editTextView.getText.toString.trim
  }

  implicit def toMyEditText(editTextView: EditText) = new MyEditText(editTextView)
}
