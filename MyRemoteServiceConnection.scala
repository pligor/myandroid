package com.pligor.myandroid

import android.os.{IBinder, Messenger}
import android.content.{Context, ComponentName, ServiceConnection}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyRemoteServiceConnection {
  //abstract

  //concrete
  protected var serviceMessengerOption: Option[Messenger] = None

  private var serviceConnectionOption: Option[ServiceConnection] = None

  //protected def getServiceConnectionOption

  protected def initServiceConnection(onRemoteServiceConnected: Messenger => Unit): ServiceConnection = {
    serviceConnectionOption = Some(
      new ServiceConnection {
        def onServiceConnected(className: ComponentName, boundService: IBinder): Unit = {
          serviceMessengerOption = Some(new Messenger(boundService))

          log log "service connected with name: " + className.toShortString

          onRemoteServiceConnected(serviceMessengerOption.get)
        }

        def onServiceDisconnected(className: ComponentName): Unit = {
          serviceMessengerOption = None

          log log "service disconnected with name: " + className.toShortString
        }
      }
    )

    serviceConnectionOption.get
  }

  protected def safeUnbindService(implicit context: Context): Unit = {
    serviceConnectionOption.foreach {
      serviceConnection =>
        assert(serviceMessengerOption.isDefined)

        serviceMessengerOption = None
        //because onServiceDisconnected gets executed only on rare situation when the service crashes etc.

        context.unbindService(serviceConnectionOption.get)

        serviceConnectionOption = None
    }
  }
}
