package com.pligor.myandroid

import android.content.Context
import android.view.View

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object ImageSizePx {
  /**
   * DO NOT CALL THIS TOO EARLY, instead use this solution: http://stackoverflow.com/questions/3591784/android-get-width-returns-0
   */
  def apply(view: View): ImageSizePx = {
    ImageSizePx(width = Pixel(view.getWidth), height = Pixel(view.getHeight))
  }
}

case class ImageSizePx(width: Pixel, height: Pixel) {
  def toDp(implicit context: Context) = new ImageSizeDp(width.toDensityPixel, height.toDensityPixel);

  def fitIfLarger(targetSize: ImageSizePx): ImageSizePx = {
    if (width > targetSize.width || height > targetSize.height) {
      val targetWidth = targetSize.width;
      val targetHeight = targetSize.height;

      val widthRatio = width.value.toFloat / targetWidth.value.toFloat

      val heightRatio = height.value.toFloat / targetHeight.value.toFloat

      val maxRatio = math.max(widthRatio, heightRatio)

      val shrinkedWidth = math.round(width.value.toFloat / maxRatio)

      val shrinkedHeight = math.round(height.value.toFloat / maxRatio)

      val rationalWidth = if (shrinkedWidth < 1) 1 else shrinkedWidth

      val rationalHeight = if (shrinkedHeight < 1) 1 else shrinkedHeight

      ImageSizePx(Pixel(rationalWidth), Pixel(rationalHeight))
    } else {
      this
    }
  }

  def getRatio = width.value.toFloat / height.value.toFloat
}

object ImageSizeDp {
  def apply(view: View)(implicit context: Context): ImageSizeDp = {
    ImageSizePx(view).toDp
  }
}

case class ImageSizeDp(width: DensityPixel, height: DensityPixel) {
  def toPx(implicit context: Context) = new ImageSizePx(width.toPixel, height.toPixel);
}
