package com.pligor.myandroid

import android.os.Parcelable.Creator
import android.os.{Parcelable, Parcel}
import scala.collection.mutable

/*
traditionally:
import android.os.Parcelable.Creator
import android.os.{Parcelable, Parcel}

object AlljoynTarget {
  final val CREATOR: Creator[AlljoynTarget] = new Creator[AlljoynTarget] {
    def createFromParcel(parcel: Parcel) = AlljoynTarget(
      parcel.readString(),
      parcel.readInt().toShort
    );

    def newArray(size: Int) = new Array[AlljoynTarget](size);
  }
}

case class AlljoynTarget(name: String, transport: Short) extends Parcelable {
  def describeContents(): Int = 0;	//use CONTENTS_FILE_DESCRIPTOR if you are passing a file descriptor

  def writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeString(name);
    parcel.writeInt(transport);
  }
}
*/


/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object CaseClassParcelableEnumeration extends Enumeration {
  val STRING = Value

  val INTEGER = Value

  val BOOLEAN = Value

  val PARCELABLE = Value

  val LIST_ANYREF = Value
}

abstract class ObjectCaseClassParcelable[T <: CaseClassParcelable](implicit manifest: Manifest[T]) {

  //abstract

  protected def getObjectReference: AnyRef

  protected def getClassReference: Class[T]

  //concrete

  private def listToObject(params: List[Any]) = {
    getClassReference.getMethods.find(
      x => x.getName == "apply" && x.isBridge
    ).get.invoke(getObjectReference, params /*this might cause issues but haven't so far*/ map (_.asInstanceOf[AnyRef]): _*).asInstanceOf[T]
  }

  final val CREATOR: Creator[T] = new Creator[T] {

    def createFromParcel(parcel: Parcel) = {

      val methodQueueSize = parcel.readInt()

      val methodQueueArray = new Array[Int](methodQueueSize)
      //methodQueueArray is empty
      parcel.readIntArray(methodQueueArray)
      //methodQueueArray is now fully written

      val dataArray = methodQueueArray.map({
        case CaseClassParcelableEnumeration.STRING => parcel.readString()

        case CaseClassParcelableEnumeration.INTEGER => parcel.readInt()

        case CaseClassParcelableEnumeration.BOOLEAN => parcel.readByte() != 0

        case CaseClassParcelableEnumeration.PARCELABLE =>
          val className = parcel.readString()

          val theClass = Class.forName(className)

          parcel.readParcelable[Parcelable](theClass.getClassLoader)

        case CaseClassParcelableEnumeration.LIST_ANYREF =>
        //val listSize = parcel.readInt()
        //java.lang.reflect.Array.newInstance(, listSize)
        //val a = new Array[Parcelable](listSize)
        //parcel.readArray()
      })

      listToObject(dataArray.toList)
    }

    def newArray(size: Int) = new Array[T](size)
  }
}

trait CaseClassParcelable extends Parcelable with Product {
  /**
   * @return use CONTENTS_FILE_DESCRIPTOR if you are passing a file descriptor
   */
  def describeContents(): Int = 0

  /* NOT ALL ARE IMPLEMENTED YET
   */
  def writeToParcel(parcel: Parcel, flags: Int): Unit = {
    //TODO drop queue and try "map" method

    val methodQueue = mutable.Queue.apply(Seq.empty[Int]: _ *)

    productIterator.foreach({
      case string: String => methodQueue.enqueue(CaseClassParcelableEnumeration.STRING.id)

      case integer: Int => methodQueue.enqueue(CaseClassParcelableEnumeration.INTEGER.id)

      case boolean: Boolean => methodQueue.enqueue(CaseClassParcelableEnumeration.BOOLEAN.id)

      case parcelable: Parcelable => methodQueue.enqueue(CaseClassParcelableEnumeration.PARCELABLE.id)

      case list: List[_] => methodQueue.enqueue(CaseClassParcelableEnumeration.LIST_ANYREF.id)

      case _ =>
        throw new ClassCastException(
          "make sure you have covered all cases for parcelables"
        )
    })

    parcel.writeInt(methodQueue.length)

    parcel.writeIntArray(methodQueue.toArray)

    productIterator.foreach({
      case string: String => parcel.writeString(string)

      case integer: Int => parcel.writeInt(integer)

      case bool: Boolean => parcel.writeByte((if (bool) 1 else 0).toByte)

      case parcelable: Parcelable =>
        parcel.writeString(parcelable.getClass.getName)

        parcel.writeParcelable(parcelable, flags)

      case list: List[AnyRef] =>
        //TODO ................. ???? how to do it ?
        //parcel.writeInt(list.length)

        //parcel.writeArray(list.toArray)

      //parcel.writeString(parcelable.getClass.getName)
      //
      //parcel.writeArray(list.toArray)

      case _ =>
        throw new ClassCastException(
          "make sure you have covered all cases for parcelables"
        )
    })
  }
}

/*
testing:

      {
        val myop = phone.operator

        log log "myop is " + myop

        val bundle = new Bundle

        bundle.putParcelable("myparce", myop)

        val newop = bundle.getParcelable[PredefinedOperator]("myparce")

        log log "newop is " + newop
      }

      {
        val beforephone = phone

        log log "beforephone is " + beforephone

        val bundle = new Bundle

        bundle.putParcelable("phone", beforephone)

        val afterphone = bundle.getParcelable[PredefinedPhoneNumber]("phone")

        log log "afterphone is " + afterphone
      }
 */
