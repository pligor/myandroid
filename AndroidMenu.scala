package com.pligor.myandroid

import org.scaloid.common.SActivity
import android.view.{ViewConfiguration, MenuItem, Menu}
import android.os.Bundle
import android.content.Context
import android.app.SearchManager
import android.support.v7.widget.SearchView
import android.widget.ToggleButton

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 *
 * For Action Bar compatibility: http://stackoverflow.com/questions/7844517/difference-between-actionbarsherlock-and-actionbar-compatibility
 *
 * sample menu:
 <?xml version="1.0" encoding="utf-8"?>
    <menu xmlns:android="http://schemas.android.com/apk/res/android">
    <!--android:alphabeticShortcut="a"-->
    <!--android:numericShortcut="1"-->
    <item
      android:id="@+id/mainMenuFacebookPage"
      android:title="@string/main_menu_facebook_page"/>
    </menu>
 */
trait AndroidMenu extends SActivity {
  protected def optionsMenuId: Int

  protected def onOptionsMenuItemSelected: PartialFunction[Int, Unit]

  protected override def onCreate(b: Bundle): Unit = {
    super.onCreate(b)

    declareMenuKeyNotAvailable()
  }

  /**
   * call invalidateOptionsMenu() if you want to destroy it and recreate it
   */
  override def onCreateOptionsMenu(menu: Menu): Boolean = {
    //menu.findItem() //to search menu item by id

    val created = super.onCreateOptionsMenu(menu)
    getMenuInflater.inflate(optionsMenuId, menu)

    /*val searchManager = getSystemService(Context.SEARCH_SERVICE).asInstanceOf[SearchManager]

    val searchView = menu.findItem(R.id.action_search).getActionView.asInstanceOf[SearchView]

    searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName))*/

    created
  }

  override def onOptionsItemSelected(item: MenuItem): Boolean = {
    val selected = super.onOptionsItemSelected(item)

    onOptionsMenuItemSelected(item.getItemId)

    selected
  }

  /**
   * must be called before set content view of activity/fragment
   */
  private def declareMenuKeyNotAvailable() = {
    try {
      val config = ViewConfiguration.get(this)
      val menuKeyField = classOf[ViewConfiguration].getDeclaredField("sHasPermanentMenuKey")

      if(Option(menuKeyField).isDefined) {
        menuKeyField.setAccessible(true)
        menuKeyField.setBoolean(config, false)
      }
    } catch {
      case e: Exception => log log "exception while trying to override menu key"
    }
  }
}
