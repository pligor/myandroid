package com.pligor.myandroid
import scala.concurrent.{ExecutionContext, Future}
import com.pligor.generic.FutureHelper._
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait JsonRequestTrait extends JsonRequestBlockingTrait {

  //abstract

  //concrete

  /**
   * @param context THIS IMPLICIT CONTEXT IS VERY CRUCIAL, SCALAJ THROWS EXCEPTION OTHERWISE
   */
  def doJsonRequest(jsonRequestParam: JsonRequestParam)
                   (op: Option[JsonResponseResult] => Unit)(implicit context: Context, executionContext: ExecutionContext): () => Boolean = {

    val (f, cancellor) = interruptableFuture({
      future: Future[Unit] =>
        super.doJsonRequest(jsonRequestParam)(op)
    })

    cancellor
  }
}