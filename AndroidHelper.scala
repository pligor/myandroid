package com.pligor.myandroid

import android.bluetooth.BluetoothAdapter
import android.nfc.NfcAdapter
import android.content.{BroadcastReceiver, IntentFilter, Intent, Context}
import android.util.{Xml, AttributeSet}
import org.xmlpull.v1.XmlPullParser
import android.content.res.Resources
import android.net.{Uri, NetworkInfo, ConnectivityManager}
import android.widget.Toast
import android.app.{PendingIntent, Activity, ActivityManager}
import scala.collection.JavaConverters._
import android.os.{Handler, Bundle, HandlerThread, Looper}
import java.util.UUID
import android.content.pm.PackageManager
import scala.collection.mutable
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber
import scala.annotation.tailrec
import android.view.{View, Gravity, ViewGroup, WindowManager}
import android.graphics.PixelFormat
import android.telephony.SmsManager

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object AndroidHelper {
  def getContentLayout(activity: Activity) = {
    activity.findViewById(android.R.id.content).asInstanceOf[ViewGroup].getChildAt(0)
  }

  private val telephonyUriPrefix = "tel"

  private val emailUriPrefix = "mailto"

  def safeUnregisterReceiver(receiver: BroadcastReceiver)
                            (implicit androidContext: Context): Unit = {
    try {
      androidContext.unregisterReceiver(receiver)
    }
    catch {
      case e: IllegalArgumentException => {
        //do nothing for the time being
      }
    }
  }

  /**
   * http://developer.android.com/reference/android/telephony/SmsManager.html#sendTextMessage%28java.lang.String,%20java.lang.String,%20java.lang.String,%20android.app.PendingIntent,%20android.app.PendingIntent%29
   */
  def sendSmsOnTheBackground(phoneNumberString: String,
                             smsMessage: String,
                             sentIntentAction: String,
                             deliveryIntentAction: String,

                             onSmsSent: Bundle => Unit,
                             onGenericFailure: Bundle => Unit,
                             onRadioOff: Bundle => Unit,
                             onNullPdu: Bundle => Unit,
                             onNoService: Bundle => Unit,

                             onDelivered: Bundle => Unit,
                             onFailedDelivery: Bundle => Unit
                              )
                            (implicit ctx: Context): BroadcastReceiver = {

    val intentFilter = {
      val filter = new IntentFilter()

      filter.addAction(sentIntentAction)

      filter.addAction(deliveryIntentAction)

      filter
    }


    val broadcastReceiver = new BroadcastReceiver {
      def onReceive(context: Context, intent: Intent): Unit = {
        intent.getAction match {
          case `sentIntentAction` =>
            getResultCode match {
              case Activity.RESULT_OK =>
                onSmsSent(intent.getExtras)

              case SmsManager.RESULT_ERROR_RADIO_OFF =>
                onRadioOff(intent.getExtras)

              case SmsManager.RESULT_ERROR_NULL_PDU =>
                onNullPdu(intent.getExtras)

              case SmsManager.RESULT_ERROR_NO_SERVICE =>
                onNoService(intent.getExtras)

              case _ => //case SmsManager.RESULT_ERROR_GENERIC_FAILURE =>
                onGenericFailure(intent.getExtras)
            }

          case `deliveryIntentAction` =>
            getResultCode match {
              case Activity.RESULT_OK =>
                onDelivered(intent.getExtras)

              case _ => //case Activity.RESULT_CANCELED
                onFailedDelivery(intent.getExtras)
            }
        }
      }
    }

    ctx.registerReceiver(broadcastReceiver, intentFilter)

    sendSmsOnTheBackground(
      phoneNumberString = phoneNumberString,
      smsMessage = smsMessage,
      sentIntentActionOption = Some(sentIntentAction),
      deliveryIntentActionOption = Some(deliveryIntentAction)
    )

    broadcastReceiver
  }

  def sendSmsOnTheBackground(phoneNumberString: String,
                             smsMessage: String,
                             sentIntentActionOption: Option[String] = None,
                             deliveryIntentActionOption: Option[String] = None)
                            (implicit ctx: Context) = {
    val sentIntentOption = sentIntentActionOption.map {
      sentIntentAction =>
        PendingIntent.getBroadcast(
        ctx, {
          val requestCode = 0
          requestCode
        },
        new Intent(sentIntentAction), {
          val flags = 0
          flags
        }
        )
    }

    val deliveryIntentOption = deliveryIntentActionOption.map {
      deliveryIntentAction =>
        //PendingIntent.getActivity() //only for activities
        PendingIntent.getBroadcast(
        ctx, {
          val requestCode = 0
          requestCode
        },
        new Intent(deliveryIntentAction), {
          val flags = 0
          flags
        }
        )
    }

    SmsManager.getDefault.sendTextMessage(
    phoneNumberString, {
      val scAddress = null
      scAddress
    },
    smsMessage,
    sentIntentOption.getOrElse(null),
    deliveryIntentOption.getOrElse(null)
    )
  }

  def callEmailApps(mail: Option[String] = None)(implicit context: Context): Unit = {
    val emailUri = Uri.parse(emailUriPrefix + ":" + mail.getOrElse(""))

    val intent = new Intent(Intent.ACTION_SENDTO).setData(emailUri)

    context.startActivity(intent)
  }

  def hasTelephony(implicit context: Context): Boolean = {
    context.getPackageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)
  }

  def phoneCall(phoneNumberString: String)(implicit context: Context): Unit = {
    val phoneUri = Uri.parse(telephonyUriPrefix + ":" + phoneNumberString)
    val intent = new Intent(Intent.ACTION_CALL).setData(phoneUri)
    context.startActivity(intent)
  }

  def bundleOfStringsToMap(bundle: Bundle): Map[String, String] = {
    val map = new mutable.HashMap[String, String]
    val keySet: Set[String] = bundle.keySet.asScala.toSet
    for (cur_key <- keySet) {
      map.put(cur_key, bundle.getString(cur_key))
    }
    map.toMap
  }

  def getAndroidConfigurationLocale(implicit context: Context) = {
    context.getResources.getConfiguration.locale
  }

  /**
   * IS BETTER TO HAVE A VARIABLE RATHER THAN XML AFFECTING THE FUNCTIONALITY OF THE APP
   */
  /*def isDebuggable(implicit context: Context): Boolean = {
    val debuggableInt = context.getApplicationInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE;
    debuggableInt != 0;
  }*/

  /**
   * http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id
   */
  def getUniqueInstallationId(implicit context: Context): String = {
    val PREF_UNIQUE_ID = "PREF_UNIQUE_ID"

    val sharedPreferences = context.getSharedPreferences(PREF_UNIQUE_ID, Context.MODE_PRIVATE)

    val uniqueIDOption = Option(sharedPreferences.getString(PREF_UNIQUE_ID, null))

    uniqueIDOption.getOrElse({
      val uniqueID = UUID.randomUUID().toString

      val editor = sharedPreferences.edit()

      editor.putString(PREF_UNIQUE_ID, uniqueID)

      editor.commit()

      uniqueID
    })
  }

  def onUIthread: Boolean = Looper.getMainLooper.getThread == Thread.currentThread()

  def onUI(segment: => Unit)(implicit activity: Activity): Unit = {
    activity.runOnUiThread(new Runnable {
      def run(): Unit = {
        segment
      }
    })
  }

  def onUIwithoutActivity(segment: => Unit)(implicit context: Context): Boolean = {
    new Handler(context.getMainLooper).post(new Runnable {
      def run(): Unit = {
        segment
      }
    })
  }

  def getHandlerLooper = {
    //create a thread with a looper
    //val busThread = new HandlerThread("AlljoynBusHandler" + UUID.randomUUID());
    val thread = new HandlerThread(UUID.randomUUID().toString)
    //start it
    thread.start()
    //create a handler which runs inside the loop of the looper
    thread.getLooper
  }

  /**
   * NOT TESTED YET
   */
  def isOn3G(implicit context: Context): Boolean = {
    val info = Option(context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager].getActiveNetworkInfo)

    info.isDefined && info.get.getTypeName.toLowerCase == "mobile"
  }

  def isAndroidConnectedOnline(implicit context: Context): Boolean = {
    val connecOption = Option(context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager])

    connecOption.exists(connec => {
      val netInfo = Option(connec.getActiveNetworkInfo)

      netInfo.exists(_.getState == NetworkInfo.State.CONNECTED)
    })
  }

  def textToClipboard(key: String, text: String)(implicit context: Context): Unit = {
    /*if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
      android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      clipboard.setText("text to clip");
    } else {
      android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
      android.content.ClipData clip = android.content.ClipData.newPlainText("text label","text to clip");
      clipboard.setPrimaryClip(clip);
    }*/

    val clipboardManager = context.getSystemService(Context.CLIPBOARD_SERVICE).asInstanceOf[android.text.ClipboardManager]

    clipboardManager.setText(text)
  }

  def getAppNameByPID(pid: Int)(implicit context: Context): Option[String] = {
    val manager = context.getSystemService(Context.ACTIVITY_SERVICE).asInstanceOf[ActivityManager]

    val processInfo = manager.getRunningAppProcesses.asScala.find(_.pid == pid)

    if (processInfo.isDefined) {
      Some(processInfo.get.processName)
    } else {
      None
    }
  }

  def showToast(resourceId: Int, args: String*)(implicit context: Context): Unit = {
    val toast = if (args.isEmpty) {
      Toast.makeText(context, resourceId, Toast.LENGTH_LONG)
    } else {
      Toast.makeText(context, context.getResources.getString(resourceId).format(args: _ *), Toast.LENGTH_LONG)
    }
    toast.show()
  }

  def getAttributeSetFromXml(xmlId: Int, tagName: String, resources: Resources): AttributeSet = {
    /*var state = XmlPullParser.END_DOCUMENT;
    do {
      try {
        state = xmlPullParser.next();
        if (state == XmlPullParser.START_TAG) {
          if (xmlPullParser.getName contains "CardView") {
            Logger(xmlPullParser.getName);
            attrs = Xml.asAttributeSet(xmlPullParser);

            state = XmlPullParser.END_DOCUMENT; //same as break :P
          }
        }
      }
    } while (state != XmlPullParser.END_DOCUMENT);*/


    /**
     * The good thing for being an internal function is that we don't need to pass tagName as a ref
     */
    @tailrec
    def getAttributeSet(xmlPullParser: XmlPullParser /*, tagName: String*/): AttributeSet = {
      val state = xmlPullParser.next()

      if (state == XmlPullParser.START_TAG &&
        (xmlPullParser.getName contains tagName)) {
        Xml.asAttributeSet(xmlPullParser)
      } else {
        if (state == XmlPullParser.END_DOCUMENT) null

        else getAttributeSet(xmlPullParser /*, tagName*/)
      }
    }

    getAttributeSet(resources.getXml(xmlId) /*, tagName*/)
  }

  def isSDcardAvailable: Boolean = {
    android.os.Environment.getExternalStorageState == android.os.Environment.MEDIA_MOUNTED
  }

  def isBluetoothSupported: Boolean = {
    BluetoothAdapter.getDefaultAdapter match {
      case null => false
      case x: BluetoothAdapter => true
    }
  }

  /**
   * Note: getDefaultAdapter with no context parameter is deprecated
   */
  def isNFCsupported(implicit context: Context): Boolean = {
    NfcAdapter.getDefaultAdapter(context) match {
      case null => false
      case x: NfcAdapter => true
    }
  }

  def isWifiSupported(implicit context: Context): Boolean = {
    //http://stackoverflow.com/questions/16723808/how-to-check-if-an-android-device-is-equipped-with-a-wifi-adapter/16723936
    context.getPackageManager.hasSystemFeature(PackageManager.FEATURE_WIFI)
  }

  def isWifiConnected(implicit context: Context): Boolean = {
    context.getSystemService(Context.CONNECTIVITY_SERVICE).asInstanceOf[ConnectivityManager].
      getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected
  }

  def getScreenSize(windowManager: WindowManager): ImageSizePx = {
    val display = windowManager.getDefaultDisplay

    ImageSizePx(width = Pixel(display.getWidth), height = Pixel(display.getHeight))
  }

  private val goldenRatio = 1.61803398875

  /**
   * Remember to have an action to remove this view
   * @param applicationContext works better with application context, that's why we have it as explicit parameter
   */
  def popupOverlayView(view: View, offset: ImageSizePx, applicationContext: Context) = {
    val params = new WindowManager.LayoutParams(
      ViewGroup.LayoutParams.WRAP_CONTENT,
      ViewGroup.LayoutParams.WRAP_CONTENT,
      WindowManager.LayoutParams.TYPE_PHONE,
      WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
      PixelFormat.TRANSLUCENT
    )

    val windowManager = applicationContext.getSystemService(Context.WINDOW_SERVICE).asInstanceOf[WindowManager]

    params.gravity = Gravity.TOP | Gravity.LEFT

    val screenSize = getScreenSize(windowManager)

    params.x = {
      val width = screenSize.width.value

      (width - math.round(width.toDouble / goldenRatio)).toInt - offset.width.value
    }

    log log "params x: " + params.x

    params.y = {
      val height = screenSize.height.value

      (height - math.round(height.toDouble / goldenRatio)).toInt - offset.height.value
    }

    log log "params y: " + params.y

    windowManager.addView(view, params)

    windowManager
  }
}
