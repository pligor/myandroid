package com.pligor.myandroid.sqlite

import android.content.{ContextWrapper, Context, ContentValues}
import android.database.Cursor
import scala.collection.mutable
import com.pligor.myandroid.log
import net.sqlcipher.database.SQLiteDatabase
import java.io.File
import java.lang

//with this sql you can get information/metadata for tables: pragma table_info(tablename in here)

//POPULATE/INITIALIZE database (not used because requires its own class): https://github.com/jgilfelt/android-sqlite-asset-helper

object MySQLiteOpenHelper {
  val invalidAutoIncrementId = 0

  val defaultIdCol = "id"

  val seperator = ", "

  val cursorBaseIndex = 0

  val invalidInsertId = -1

  def validateInsertion(insertResponse: Long) = insertResponse != invalidInsertId

  def genBinders(len: Int) = ("?," * len).dropRight(1)

  val commentBegin = "/*"
  val commentEnd = "*/"

  /**
   * http://stackoverflow.com/questions/18710195/net-sqlcipher-database-sqliteexception-while-opening-database-after-resuming-app
   */
  def loadSqlcipherLibs(implicit context: Context) = {
    SQLiteDatabase.loadLibs(context)
  }

  private class UnsupportedContentValueException extends Exception

  def createContentValues(args: (String, Any)*): ContentValues = {
    val contentValues = new ContentValues()

    args.foreach {
      tuple =>
        val (key, value) = tuple

        value match {
          case x: Array[Byte] => contentValues.put(key, x)
          case x: Boolean => contentValues.put(key, new lang.Boolean(x))
          case x: Byte => contentValues.put(key, new lang.Byte(x))
          case x: Double => contentValues.put(key, new lang.Double(x))
          case x: Float => contentValues.put(key, new lang.Float(x))
          case x: Int => contentValues.put(key, new lang.Integer(x))
          case x: Long => contentValues.put(key, new lang.Long(x))
          case x: Short => contentValues.put(key, new lang.Short(x))
          case x: String => contentValues.put(key, new lang.String(x))
          case x if x == null => contentValues.putNull(key)
          case _ => throw new UnsupportedContentValueException
        }
    }

    contentValues
  }
}

trait MySQLiteOpenHelperTrait[A] {
  //abstract
  def rawExecSQLdb(db: A, sql: String): Unit

  def execSQLdb(db: A, sql: String, bindArgs: Array[AnyRef]): Unit

  def execSQLdb(db: A, sql: String): Unit

  def rawQueryDb(db: A, query: String, args: Array[String] = null): Cursor

  def getVersion(db: A): Int

  def closeDb(db: A): Unit

  def grabReadableDatabase: A

  protected def grabWritableDatabase: A

  def databaseName: String

  def insertDb(db: A, tableName: String, contentValues: ContentValues): Long

  //concrete

  def workWithCursor[T](cursor: Cursor)(op: (Cursor) => T): T = {
    try {
      op(cursor)
    } finally {
      cursor.close()
    }
  }

  def getDBversion = {
    workWithReadableDatabase {
      db =>
        getVersion(db)
      //db.getVersion;
    }
  }

  protected def populator(insert: (String, ContentValues) => Long)(implicit context: Context): Unit

  protected def getContextWrapper: ContextWrapper

  def reset(toPopulate: Boolean = false)(implicit context: Context): Unit = {
    log log "before try deletion of db"
    val deleted: Boolean = getContextWrapper.deleteDatabase(databaseName)
    log log (if (deleted) "deleted" else "not deleted")

    closeDb(grabReadableDatabase) // simply to trigger create methods if necessary ;)

    if (toPopulate) {
      populator(insert)
      log log "finished populating"
    }
  }

  /**
   * ONLY IF TABLE HAS AN AUTOINCREMENT PRIMARY KEY AND IF THE NEW ID IS GENERATED AUTOMATICALLY
   * THEN THE RETURNED VALUE IS THAT ID
   * otherwise it gets returned the number 1 for success
   * in all situations we get the number -1 for failure
   */
  def insert(tableName: String, contentValues: ContentValues): Long = {
    workWithWritableDatabase {
      db =>
        enableForeignKeys(db)
        insertDb(db, tableName, contentValues)
    }
  }

  /**
   * @param db closing the database must happen from outside
   */
  private def enableForeignKeys(db: A) = {
    execSQLdb(db, "PRAGMA foreign_keys = 1")
  }

  def updateByCompoundId(tableName: String,
                         compoundId: Map[String, Long],
                         contentValues: ContentValues): Boolean = {
    val rowsAffected = workWithWritableDatabase {
      db =>
        enableForeignKeys(db)

        val whereClause = compoundId.map((x: (String, Long)) => x._1 + " = ?").mkString(" AND ")

        val whereArgs = compoundId.map((x: (String, Long)) => x._2.toString).toArray

        updateDb(db, tableName, contentValues, whereClause, whereArgs)
    }

    rowsAffected == 1
  }

  /**
   * @return number of rows affected
   */
  def updateByArgs(tableName: String,
                   args: Map[String, String],
                   contentValues: ContentValues): Int = {
    workWithWritableDatabase {
      db =>
        enableForeignKeys(db)

        updateDb(db,
          tableName,
          contentValues,
          whereClause = args.map((x: (String, String)) => x._1 + " = ?").mkString(" AND "),
          whereArgs = args.map((x: (String, String)) => x._2).toArray
        )
    }
  }

  def updateDb(db: A, tableName: String, contentValues: ContentValues, whereClause: String, whereArgs: Array[String]): Int

  def deleteByColumn(tableName: String, columnName: String, values: Array[AnyRef]) = {
    val binders = MySQLiteOpenHelper.genBinders(values.length)

    workWithWritableDatabase {
      db =>
        enableForeignKeys(db)
        execSQLdb(db,
          """
            |DELETE
            |FROM %s
            |WHERE %s IN (%s)
          """.stripMargin.format(tableName, columnName, binders), values)
    }
  }

  /**
   * @return the number of rows affected if a whereClause is passed in, 0 otherwise
   */
  def deleteByArgs(tableName: String, args: Map[String, String]): Int = {
    workWithWritableDatabase {
      db =>
        enableForeignKeys(db)

        val whereClause = args.map((x: (String, String)) => x._1 + " = ?").mkString(" AND ")

        val whereArgs = args.map((x: (String, String)) => x._2).toArray

        deleteDb(db, tableName, whereClause, whereArgs)
    }
  }

  def deleteByCompoundId(tableName: String,
                         compoundId: Map[String, Long]): Boolean = {
    val rowsDeleted = workWithWritableDatabase {
      db =>
        enableForeignKeys(db)

        val whereClause = compoundId.map((x: (String, Long)) => x._1 + " = ?").mkString(" AND ")

        val whereArgs = compoundId.map((x: (String, Long)) => x._2.toString).toArray

        deleteDb(db, tableName, whereClause, whereArgs)
    }
    rowsDeleted == 1
  }

  def deleteDb(db: A, tableName: String, whereClause: String, whereArgs: Array[String]): Int

  def updateById(tableName: String,
                 modelId: Long,
                 contentValues: ContentValues,
                 columnName: String): Boolean = {
    updateByCompoundId(tableName, Map(columnName -> modelId), contentValues)
  }

  def deleteById(tableName: String,
                 modelId: Long,
                 columnName: String): Boolean = {
    deleteByCompoundId(tableName, Map(columnName -> modelId))
  }

  def getSingleScalar[T](query: String, args: Array[String] = null)(implicit m: Manifest[T]): Option[T] = {
    workWithReadableDatabase {
      db =>
        workWithCursor(rawQueryDb(db, query, args)) {
          cursor =>
            if (cursor.moveToFirst()) {
              if (cursor.isNull(MySQLiteOpenHelper.cursorBaseIndex)) {
                None
              } else {
                Some(getSingle(cursor))
              }
            } else {
              None
            }
        }
    }
  }

  private def getSingle[T](cursor: Cursor)(implicit m: Manifest[T]) = {
    getCursorMethodByType[T](cursor).apply(MySQLiteOpenHelper.cursorBaseIndex).asInstanceOf[T]
  }

  def getScalars[T](query: String, args: Array[String] = null)(implicit m: Manifest[T]): Seq[T] = {
    workWithReadableDatabase {
      db =>
        workWithCursor(rawQueryDb(db, query, args)) {
          cursor =>
            moveAlongAllRows(cursor) {
              c =>
                getSingle(c)
            }
        }
    }
  }

  private def moveAlongAllRows[T](cursor: Cursor)(factory: (Cursor) => T): Seq[T] = {
    if (cursor.moveToFirst()) {
      val buffer = mutable.Buffer.empty[T]
      do {
        buffer += factory(cursor)
      } while (cursor.moveToNext())
      buffer.toSeq
    }
    else {
      Seq.empty[T]
    }
  }

  def getModelsByQuery[T](queryWITHargs: String, args: Array[String])(factory: (Cursor) => T): Seq[T] = {
    workWithReadableDatabase {
      db =>
        workWithCursor(rawQueryDb(db, queryWITHargs, args)) {
          moveAlongAllRows(_)(factory)
        }
    }
  }

  def getModelsByQuery[T](queryWithOUTargs: String)(factory: (Cursor) => T): Seq[T] = {
    getModelsByQuery(queryWithOUTargs, null)(factory)
  }

  /**
   * sample usage: getCursorMethodByType[A](cursor).apply(DatabaseHandler.cursorBaseIndex).asInstanceOf[A]
   *
   * If column value is null AND type is String then it will return null (indeed it does not crash)
   */
  private def getCursorMethodByType[T](cursor: Cursor)(implicit m: Manifest[T]): (Int) => Any = {
    //NOTE: for some reason we must first store the manifests in vals before doing pattern matching
    val arrayByteManifest = manifest[Array[Byte]]
    val doubleManifest = manifest[Double]
    val floatManifest = manifest[Float]
    val intManifest = manifest[Int]
    val longManifest = manifest[Long]
    val shortManifest = manifest[Short]
    val stringManifest = manifest[String]

    manifest[T] match {
      case `arrayByteManifest` => cursor.getBlob
      case `doubleManifest` => cursor.getDouble
      case `floatManifest` => cursor.getFloat
      case `intManifest` => cursor.getInt
      case `longManifest` => cursor.getLong
      case `shortManifest` => cursor.getShort
      case `stringManifest` => cursor.getString
      case _ => throw new UnsupportedCursorMethodTypeException
    }
  }

  private class UnsupportedCursorMethodTypeException extends Exception

  def getModelByQuery[T](queryWITHargs: String, args: Array[String])(factory: (Cursor) => T): Option[T] = {
    workWithReadableDatabase {
      db =>
        cursor2model(
          rawQueryDb(db, queryWITHargs, args)
        )(factory)
    }
  }

  private def cursor2model[T](cursor: Cursor)(factory: (Cursor) => T): Option[T] = {
    workWithCursor(cursor) {
      cursor =>
        if (cursor.moveToFirst()) {
          val optionA = Some(factory(cursor))
          assert(!cursor.moveToNext(), "should have been only one row returned")
          optionA
        }
        else {
          None
        }
    }
  }

  def getModelByQuery[T](queryWithOUTargs: String)(factory: (Cursor) => T): Option[T] = {
    getModelByQuery(queryWithOUTargs, null)(factory)
  }

  def geyByArgs() = {
    workWithReadableDatabase {
      db =>

    }
  }

  /**
   * NOTE: only works for models with id column
   */
  def getById[T](tableName: String, modelId: Long, columnName: String)(factory: (Cursor) => T): Option[T] = {
    workWithReadableDatabase {
      db =>
      // Cursor cursor=db.query(TABLE_CONTACTS, new String[] { // KEY_ID, // KEY_NAME, // KEY_PH_NO // }, KEY_ID + "=?", new String[] { //
      // String.valueOf(id)
      // }, null, null, null, null);
        val sql = "SELECT * FROM " + tableName + " WHERE " + columnName + " = ?"
        val selectionArgs = Array[String](modelId.toString)
        cursor2model(
          rawQueryDb(db, sql, selectionArgs)
        )(factory)
    }
  }

  def getNextAutoIncrementOption(tableName: String): Option[Long] = {
    workWithReadableDatabase {
      db =>
        val query = "SELECT * FROM SQLITE_SEQUENCE WHERE name = ?"
        val selectionArgs = Array[String](tableName)

        workWithCursor(rawQueryDb(db, query, selectionArgs)) {
          cursor =>
            if (cursor.moveToFirst()) {
              val nextAutoIncrement = cursor.getLong(cursor.getColumnIndexOrThrow("seq"))
              assert(!cursor.moveToNext(), "should have been only one row returned")
              Some(nextAutoIncrement)
            }
            else {
              None
            }
        }
    }
  }

  def getAll[T](tableName: String)(factory: (Cursor) => T): Seq[T] = {
    getModelsByQuery("SELECT * FROM " + tableName)(factory)
  }

  /**
   * REMEMBER TO CLOSE THE CURSOR AFTERWARDS
   */
  def getCursorAll(tableName: String, uniqueIdColumn: String = "id"): Cursor = {
    val query = "SELECT " + uniqueIdColumn + " AS _id, * FROM " + tableName
    val db = grabReadableDatabase
    rawQueryDb(db, query)
  }

  def isEmpty(tableName: String): Boolean = getCount(tableName) == 0

  def getCount(tableName: String, criteriaOption: Option[(String, String)] = None): Long = {
    //log log "we are getting the count for the table name: " +  tableName;
    val sql = "SELECT COUNT(*) FROM " + tableName + (
      if (criteriaOption.isDefined) {
        " WHERE " + criteriaOption.get._1 + " = ?"
      }
      else {
        ""
      }
      )
    //Logger("sql: " + sql);
    getSingleScalar[Long](sql, {
      if (criteriaOption.isDefined) {
        //Logger("array: " + Array[String](criteriaOption.get._2).mkString(" "));
        Array[String](criteriaOption.get._2)
      }
      else null
    }
    ).get
  }

  /**
   * i THINK it works only with a writeable database as input
   */
  private def getAutoIncrement(db: A): Long = {
    val sql: String = "SELECT last_insert_rowid()"
    workWithCursor(rawQueryDb(db, sql)) {
      cursor =>
        if (cursor.moveToFirst()) {
          cursor.getInt(MySQLiteOpenHelper.cursorBaseIndex)
        }
        else {
          MySQLiteOpenHelper.invalidAutoIncrementId
        }
    }
  }

  def tableExists(tableName: String): Boolean = {
    workWithReadableDatabase {
      db =>
        getSingleScalar[Int](
          query = """SELECT EXISTS(SELECT name FROM sqlite_master WHERE type = 'table' AND name = ?)""",
          args = Array[String](tableName)
        ).get == 1
    }
  }

  def exists(tableName: String, value: String, columnName: String): Boolean = {
    workWithReadableDatabase {
      db =>
        getSingleScalar[Int](
          query = s"""SELECT EXISTS(SELECT * FROM $tableName WHERE $columnName = ?)""",
          Array[String](value)
        ).exists(_ == 1)
    }
  }

  def exists(tableName: String, modelId: Long, columnName: String): Boolean = {
    workWithReadableDatabase {
      db =>
        val sql = "SELECT COUNT(*) FROM " + tableName + " WHERE " + columnName + " = ?"
        val selectionArgs = Array[String](modelId.toString)
        getSingleScalar[Long](sql, selectionArgs).get == 1
    }
  }

  def workWithReadableDatabase[T](op: (A) => T): T = {
    val db = grabReadableDatabase
    try {
      op(db)
    } finally {
      closeDb(db)
    }
  }

  def workWithWritableDatabase[T](op: (A) => T): T = {
    val db = grabWritableDatabase
    try {
      op(db)
    } finally {
      closeDb(db)
    }
  }

  /**
   * @return None if the statement contains comments
   *         Some(boolean) to tell if the table has one and only one auto increment or not
   */
  def isAutoIncrement(tableName: String): Option[Boolean] = {
    workWithReadableDatabase {
      db =>
        val sql =
          """SELECT sql
            |FROM sqlite_master
            |WHERE type = 'table'
            |  AND name = ?
            |  AND sql LIKE '%AUTOINCREMENT%'
            |  """.stripMargin

        val selectionArgs = Array[String](tableName)

        val createStatementOption = getSingleScalar[String](sql, selectionArgs)

        if (createStatementOption.isDefined) {
          val createStatement = createStatementOption.get
          if (createStatement.contains(MySQLiteOpenHelper.commentBegin) ||
            createStatement.contains(MySQLiteOpenHelper.commentEnd)) {
            None
          } else {
            Some( """(?i)autoincrement""".r.findAllIn(createStatement).length == 1)
          }
        } else {
          Some(false)
        }
    }
  }
}

abstract class MySQLiteOpenHelper(val dbName: String,
                                  val dbVersion: Int,
                                  val creator: (android.database.sqlite.SQLiteDatabase) => Unit)(implicit val context: Context)
  extends android.database.sqlite.SQLiteOpenHelper(context, dbName, null, dbVersion)
  with MySQLiteOpenHelperTrait[android.database.sqlite.SQLiteDatabase] {

  //we are saving in default database, if you want to save in SD card, refactor similarly to secure sqlite open helper (few lines below)
  protected def getContextWrapper = new ContextWrapper(context)

  def getDatabaseFile: File = getContextWrapper.getDatabasePath(dbName)

  def grabReadableDatabase = getReadableDatabase

  protected def grabWritableDatabase = getWritableDatabase

  val databaseName: String = dbName

  def deleteDb(db: android.database.sqlite.SQLiteDatabase, tableName: String, whereClause: String, whereArgs: Array[String]): Int = {
    db.delete(tableName, whereClause, whereArgs)
  }

  def updateDb(db: android.database.sqlite.SQLiteDatabase, tableName: String, contentValues: ContentValues, whereClause: String, whereArgs: Array[String]): Int = {
    db.update(tableName, contentValues, whereClause, whereArgs)
  }

  def execSQLdb(db: android.database.sqlite.SQLiteDatabase,
                sql: String, bindArgs: Array[AnyRef]): Unit = {
    db.execSQL(sql, bindArgs)
  }

  def execSQLdb(db: SQLiteDatabase, sql: String): Unit = {
    db.execSQL(sql)
  }

  def rawQueryDb(db: android.database.sqlite.SQLiteDatabase, query: String, args: Array[String] = null): Cursor = {
    db.rawQuery(query, args)
  }

  def insertDb(db: android.database.sqlite.SQLiteDatabase, tableName: String, contentValues: ContentValues) = {
    db.insert(tableName, null, contentValues)
  }

  def getVersion(db: android.database.sqlite.SQLiteDatabase): Int = {
    db.getVersion
  }

  def closeDb(db: android.database.sqlite.SQLiteDatabase): Unit = {
    db.close()
  }

  def onCreate(db: android.database.sqlite.SQLiteDatabase): Unit = {
    creator(db)
  }

  def rawExecSQLdb(db: net.sqlcipher.database.SQLiteDatabase, sql: String): Unit = {
    db.rawExecSQL(sql)
  }
}

/**
 * https://guardianproject.info/code/sqlcipher/
 * https://github.com/sqlcipher/android-database-sqlcipher
 */
abstract class MySQLiteOpenHelperSecure(val dbPath: String,
                                        val dbName: String,
                                        val dbVersion: Int,
                                        val dbPass: String,
                                        val creator: (net.sqlcipher.database.SQLiteDatabase) => Unit)(implicit val context: Context)
  extends net.sqlcipher.database.SQLiteOpenHelper(new DatabaseContext(dbPath), dbName, null, dbVersion)
  with MySQLiteOpenHelperTrait[net.sqlcipher.database.SQLiteDatabase] {

  protected def getContextWrapper: ContextWrapper = new DatabaseContext(dbPath)

  def getDatabaseFile: File = getContextWrapper.getDatabasePath(dbName)

  def grabReadableDatabase = getReadableDatabase(dbPass)

  protected def grabWritableDatabase = getWritableDatabase(dbPass)

  def onCreate(db: net.sqlcipher.database.SQLiteDatabase): Unit = {
    creator(db)
    log log "tables were created successfully"
  }

  def deleteDb(db: net.sqlcipher.database.SQLiteDatabase, tableName: String, whereClause: String, whereArgs: Array[String]): Int = {
    db.delete(tableName, whereClause, whereArgs)
  }

  def updateDb(db: net.sqlcipher.database.SQLiteDatabase, tableName: String, contentValues: ContentValues, whereClause: String, whereArgs: Array[String]): Int = {
    db.update(tableName, contentValues, whereClause, whereArgs)
  }

  def rawQueryDb(db: net.sqlcipher.database.SQLiteDatabase, query: String, args: Array[String]): Cursor = {
    db.rawQuery(query, args)
  }

  def insertDb(db: net.sqlcipher.database.SQLiteDatabase, tableName: String, contentValues: ContentValues): Long = {
    db.insert(tableName, null, contentValues)
  }

  val databaseName: String = dbName

  def closeDb(db: net.sqlcipher.database.SQLiteDatabase): Unit = {
    db.close()
  }

  def getVersion(db: net.sqlcipher.database.SQLiteDatabase): Int = {
    db.getVersion
  }

  def execSQLdb(db: net.sqlcipher.database.SQLiteDatabase,
                sql: String, bindArgs: Array[AnyRef]): Unit = {
    db.execSQL(sql, bindArgs)
  }

  def execSQLdb(db: SQLiteDatabase, sql: String): Unit = {
    db.execSQL(sql)
  }

  def rawExecSQLdb(db: net.sqlcipher.database.SQLiteDatabase, sql: String): Unit = {
    db.rawExecSQL(sql)
  }
}

