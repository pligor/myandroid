package com.pligor.myandroid.sqlite

import android.content.Context
import models.DatabaseHandler

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait OrderNumberObject {
  //abstract

  def TABLE_NAME: String

  def ORDERING_COLUMN: String

  //concrete

  val INVALID_ORDER_NUMBER = -1

  val ORDER_LOWER_BOUND = 0

  def getNextOrderNumber(implicit context: Context): Option[Int] = {
    val colName = "nextOrderNumber"

    val query = "SELECT MAX(" + ORDERING_COLUMN + ")+1 AS " + colName + " FROM " + TABLE_NAME

    DatabaseHandler.getInstance.getSingleScalar[Int](query)
  }

  def getTop(implicit context: Context): Option[MyModel]


  private def execute(fromItem: OrderNumber, toItem: OrderNumber)
                     (curFunc: (OrderNumber) => Unit)(implicit context: Context): Unit = {
    val toItemNumber = toItem.orderNumber

    val model = fromItem.setOrderNumber(INVALID_ORDER_NUMBER)

    curFunc(toItem)

    model.setOrderNumber(toItemNumber)
  }

  def lowerToHigher(fromItem: OrderNumber, toItem: OrderNumber)
                   (implicit context: Context): Unit = {
    require(fromItem.orderNumber < toItem.orderNumber)

    def func(curModel: OrderNumber): Unit = {
      val nextOption = curModel.getNext

      if (nextOption.isDefined && curModel.orderNumber - 1 == nextOption.get.orderNumber) {
        func(nextOption.get)
      }

      curModel.decreaseOrderNumberByOne
    }

    execute(fromItem, toItem)(func)
  }

  def higherToLower(fromItem: OrderNumber, toItem: OrderNumber)
                   (implicit context: Context): Unit = {
    require(fromItem.orderNumber > toItem.orderNumber)

    def func(curModel: OrderNumber): Unit = {
      val prevOption = curModel.getPrev

      if (prevOption.isDefined && curModel.orderNumber + 1 == prevOption.get.orderNumber) {
        func(prevOption.get)
      }

      curModel.increaseOrderNumberByOne
    }

    /*def funcImperative(model: OrderNumber) {
      val buffer = new mutable.ArrayBuffer[OrderNumber];

      var curModel = model;
      var prevOption = curModel.getPrev;
      while (prevOption.isDefined && curModel.orderNumber + 1 == prevOption.get.orderNumber) {
        buffer += curModel;

        curModel = prevOption.get;
        prevOption = curModel.getPrev;
      }
      buffer += curModel;

      buffer.reverse.foreach {
        myModel =>
          myModel.increaseOrderNumberByOne;
      }
    }*/

    execute(fromItem, toItem)(func)
  }
}

/**
 * Remember that current convention is that next is the lower order number,
 * while previous is the higher order number
 */
trait OrderNumber {
  //abstract

  def orderNumber: Int

  def setOrderNumber(newOrderNumber: Int)(implicit context: Context): OrderNumber

  def getNext(implicit context: Context): Option[OrderNumber]

  def getPrev(implicit context: Context): Option[OrderNumber]

  //concrete

  def decreaseOrderNumberByOne(implicit context: Context): OrderNumber = {
    setOrderNumber(orderNumber - 1)
  }

  def increaseOrderNumberByOne(implicit context: Context): OrderNumber = {
    setOrderNumber(orderNumber + 1)
  }
}