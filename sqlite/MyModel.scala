package com.pligor.myandroid.sqlite

import android.database.Cursor
import android.content.{ContentValues, Context}
import models.DatabaseHandler

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
trait MyModelObject[T <: MyModel] {
  //abstract

  def TABLE_NAME: String

  def createFromCursor(cursor: Cursor): T

  //concrete

  def getCount(implicit context: Context) = DatabaseHandler.getInstance.getCount(TABLE_NAME)

  def getAll(implicit context: Context): Seq[T] = DatabaseHandler.getInstance.getAll(TABLE_NAME)(createFromCursor)

  def getById(modelId: Long)(implicit context: Context): Option[T] = {
    DatabaseHandler.getInstance.getById(TABLE_NAME, modelId, columnName = ID_COLUMN_NAME)(createFromCursor)
  }

  val ID_COLUMN_NAME = MySQLiteOpenHelper.defaultIdCol

  def getCursorAll(implicit context: Context): Cursor = DatabaseHandler.getInstance.getCursorAll(TABLE_NAME)

  def exists(modelId: Long)(implicit context: Context): Boolean = {
    DatabaseHandler.getInstance.exists(TABLE_NAME, modelId, columnName = ID_COLUMN_NAME)
  }
}

abstract class MyModel(val id: Option[Long]) {
  //abstract

  val TABLE_NAME: String

  val ID_COLUMN_NAME: String

  protected def generateContentValues: ContentValues

  //concrete

  def isOld = id.isDefined

  def isNew = !isOld

  def isIdAutoIncrement(implicit context: Context): Boolean = {
    DatabaseHandler.getInstance.isAutoIncrement(TABLE_NAME).get
  }

  def update(implicit context: Context): Boolean = {
    require(id.isDefined)
    DatabaseHandler.getInstance.updateById(TABLE_NAME, id.get, generateContentValues, ID_COLUMN_NAME)
  }

  def delete(implicit context: Context) = {
    require(id.isDefined)

    DatabaseHandler.getInstance.deleteById(TABLE_NAME, id.get, ID_COLUMN_NAME)
  }

  def insert(implicit context: Context) = {
    if (isIdAutoIncrement) {
      require(!id.isDefined)
    }
    DatabaseHandler.getInstance.insert(TABLE_NAME, generateContentValues)
  }
}
