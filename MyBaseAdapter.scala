package com.pligor.myandroid

import android.widget.{ExpandableListView, BaseExpandableListAdapter, ListView, BaseAdapter}
import android.view.View

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait MyBaseAdapter {
  this: BaseAdapter =>
  //no need to extend base adapter

  //abstract

  protected def listView: ListView

  //concrete

  protected def onBeforeUpdate() = listView.requestLayout()
}

trait MyBaseExpandableListAdapter {
  this: BaseExpandableListAdapter =>

  //abstract

  protected def expandableListView: ExpandableListView

  //concrete

  protected def onBeforeUpdate() = expandableListView.requestLayout()

  /**
   * http://www.kylemallette.com/android/refreshing-lists-part2
   */
  protected def getAllVisibleViews: IndexedSeq[Option[(View, Either[Int, (Int, Int)])]] = {
    // These are both implemented in AdapterView which means
    // they're flat list positions

    val firstVis = expandableListView.getFirstVisiblePosition

    val lastVis = expandableListView.getLastVisiblePosition

    val visibleRange = firstVis to lastVis

    visibleRange.map {
      flatListPosition =>
        Option(expandableListView.getChildAt(flatListPosition)).flatMap {
          view =>
            val packedPosition = expandableListView.getExpandableListPosition(flatListPosition)

            // Using our static helpers to unpack what type of position this item represents.
            val packedPositionType = ExpandableListView.getPackedPositionType(packedPosition)

            if (packedPositionType != ExpandableListView.PACKED_POSITION_TYPE_NULL) {

              /* If the packedPosition type isn't NULL then we know there has to be at least
               * a group position value. If it's a child item, we'll additionally have a
               * child position value */
              val groupPosition = ExpandableListView.getPackedPositionGroup(packedPosition)

              if (packedPositionType == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {

                val childPosition = ExpandableListView.getPackedPositionChild(packedPosition)

                Some(
                  (view, Right((groupPosition, childPosition)))
                )
              } else {
                Some(
                  (view, Left(groupPosition))
                )
              }
            } else {
              None
            }
        }
    }
  }
}
