package com.pligor.myandroid

import android.content.Context
import android.preference.PreferenceManager
import com.pligor.mycrypto.crypto.AES
import com.pligor.mycrypto.protocol.defaults._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * repeat the preferences' keys so that we have a more concrete reference
 */
abstract class MyPreferencesCase[INNER_TYPE, OUTER_TYPE](val AESkey: Option[String]) {
  //abstract

  def preferenceKey: String

  def defaultValue: INNER_TYPE

  def getValue(implicit context: Context): OUTER_TYPE

  def setValue(newValue: OUTER_TYPE)(implicit context: Context): Boolean

  //concrete

  def isDefault(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]) = {
    getInnerValue.asInstanceOf[INNER_TYPE] == defaultValue
  }

  def isEmpty(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]) = isDefault

  private lazy val finalPreferenceKey: String = AESkey.map {
    secret =>
      AES.encryptToString(preferenceKey, secret)
  }.getOrElse(preferenceKey)

  def clear(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]): Boolean = {
    setInnerValue(defaultValue)
  }

  /**
   * same as clear
   */
  def reset(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]): Boolean = {
    clear
  }

  private val booleanManifest = manifest[Boolean]
  private val floatManifest = manifest[Float]
  private val intManifest = manifest[Int]
  private val longManifest = manifest[Long]
  private val stringManifest = manifest[String]

  protected def getInnerValue(implicit context: Context, innerTypeManifest: Manifest[INNER_TYPE]): Any = {
    val defaultPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    innerTypeManifest match {
      case `booleanManifest` => defaultPrefs.getBoolean(finalPreferenceKey, defaultValue.asInstanceOf[Boolean])
      case `floatManifest` => defaultPrefs.getFloat(finalPreferenceKey, defaultValue.asInstanceOf[Float])
      case `intManifest` => defaultPrefs.getInt(finalPreferenceKey, defaultValue.asInstanceOf[Int])
      case `longManifest` => defaultPrefs.getLong(finalPreferenceKey, defaultValue.asInstanceOf[Long])
      case `stringManifest` => defaultPrefs.getString(finalPreferenceKey, defaultValue.asInstanceOf[String])
    }
  }

  protected def setInnerValue(newValue: INNER_TYPE)
                             (implicit context: Context,
                              innerTypeManifest: Manifest[INNER_TYPE]): Boolean = {
    val defaultPrefsEditor1 = PreferenceManager.getDefaultSharedPreferences(context).
      edit().
      remove(finalPreferenceKey)

    val defaultPrefsEditor2 = innerTypeManifest match {
      case `booleanManifest` => defaultPrefsEditor1.putBoolean(finalPreferenceKey, newValue.asInstanceOf[Boolean])
      case `floatManifest` => defaultPrefsEditor1.putFloat(finalPreferenceKey, newValue.asInstanceOf[Float])
      case `intManifest` => defaultPrefsEditor1.putInt(finalPreferenceKey, newValue.asInstanceOf[Int])
      case `longManifest` => defaultPrefsEditor1.putLong(finalPreferenceKey, newValue.asInstanceOf[Long])
      case `stringManifest` => defaultPrefsEditor1.putString(finalPreferenceKey, newValue.asInstanceOf[String])
    }

    defaultPrefsEditor2.commit()
  }
}

