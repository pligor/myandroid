package com.pligor.myandroid

import android.app.Activity
import android.os.Bundle
import android.view.{WindowManager, Window}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait FullScreenActivity extends Activity {
  override def onCreate(savedInstanceState: Bundle): Unit = {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
  }
}
