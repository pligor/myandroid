package com.pligor.myandroid

import android.util.Log
import android.content.Context
import components.DebuggableTrait

/**
 * Created with IntelliJ IDEA.
 * User: pligor
 */
object log extends DebuggableTrait {
  def log(msg: Any)/*(implicit context: Context)*/: Unit = {
    if(isDebuggable) {
      Log.i("pl", msg.toString);
    }
  }

  def apply(msg: Any)(implicit context: Context) = log(msg);
}
