package com.pligor.myandroid

import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
case class DensityPixel(value: Int) extends Ordered[DensityPixel] {
  require(value >= 0, s"value really is $value")

  /**
   * TESTED
   * This method convets dp unit to equivalent device specific value in pixels.
   *
   * @param context Context to get resources and device specific display metrics
   * @return A float value to represent Pixels equivalent to dp according to device
   */
  def toPixel(implicit context: Context) = {
    val displayMetrics = context.getResources.getDisplayMetrics
    //val answer1 = value * (displayMetrics.densityDpi / 160.toFloat)

    val answer2 = value * displayMetrics.density

    //actually is not always the same!
    //assert(answer1 == answer2, "answer1 is: " + answer1 + " and answer2 is: " + answer2)

    new Pixel(answer2.toInt)
  }

  def compare(that: DensityPixel): Int = {
    this.value - that.value
  }
}

case class Pixel(value: Int) extends Ordered[Pixel]  {
  require(value >= 0, s"value really is $value")

  def toDensityPixel(implicit context: Context) = {
    val displayMetrics = context.getResources.getDisplayMetrics

    val answer1 = value / displayMetrics.density

    //val answer2 = value / (displayMetrics.densityDpi / 160.toFloat)

    //assert(answer1 == answer2, "answer1 is: " + answer1 + " and answer2 is: " + answer2)

    new DensityPixel(answer1.toInt)
  }

  def compare(that: Pixel): Int = {
    this.value - that.value
  }
}
