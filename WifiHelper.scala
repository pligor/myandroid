package com.pligor.myandroid

import android.net.wifi.{WifiManager, WifiConfiguration}
import android.content.Context
import scala.collection.JavaConverters._
import com.pligor.myandroid.AndroidHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object WifiHelper {
  private val INVALID_NETWORK_ID = -1;

  def getWifiMacAddress(implicit context: Context): Option[String] = {
    getWifiManager.map(_.getConnectionInfo.getMacAddress);
  }

  def safeTurnOnWifi(implicit wifiManager: WifiManager): Boolean = {
    if (wifiManager.getWifiState == WifiManager.WIFI_STATE_DISABLED) {
      wifiManager.setWifiEnabled(true);
    } else {
      false;
    }
    /*if (wifiManager.isWifiEnabled) {
      false;
    } else {
      wifiManager.setWifiEnabled(true);
    }*/
  }

  def safeTurnOffWifi(implicit wifiManager: WifiManager): Boolean = {
    if (wifiManager.getWifiState == WifiManager.WIFI_STATE_ENABLED) {
      wifiManager.setWifiEnabled(false);
    } else {
      false;
    }
    /*if (!wifiManager.isWifiEnabled) {
      false;
    } else {
      wifiManager.setWifiEnabled(false);
    }*/
  }

  def isWifiAvailable(implicit context: Context, wifiManager: Option[WifiManager]) = {
    isWifiSupported && wifiManager.get.isWifiEnabled;
  }

  object WifiProtocol extends Enumeration {
    val WPA_WPA2 = Value;
    val WEP = Value;
    val OPEN = Value;
  }

  def getWifiManager(implicit context: Context) = Option(context.getSystemService(Context.WIFI_SERVICE).asInstanceOf[WifiManager]);

  /**
   * Overwrites existing configurations
   * NOTE: we cannot have both WEP and WPA/WPA2 in same configuration
   */
  def connectToWifiNetwork(unquotedSSID: String, unquotedPass: String, protocol: WifiProtocol.Value)
                          (implicit context: Context): Boolean = {
    val quote = "\"";
    implicit val wifiManager = context.getSystemService(Context.WIFI_SERVICE).asInstanceOf[WifiManager];

    val networkSSID = quote + unquotedSSID + quote;

    removeNetworksBySSID(networkSSID);

    /*val SSIDs = networksWithSSID.map(_.SSID);
    //this have to be enforced to be as unique as we like
    val networkId = if (SSIDs.contains(networkSSID)) {
      Logger("ssid is known");
      val networkOption = networksWithSSID.find(_.SSID == networkSSID);
      assert(networkOption.isDefined);
      networkOption.get.networkId;
    } else {*/
    val networkId = {
      //Logger("ssid is NOT known or unknown :P");

      val networkPass = quote + unquotedPass + quote;
      val conf = new WifiConfiguration;

      conf.hiddenSSID = true;

      conf.SSID = networkSSID;

      val finalConfiguration = protocol match {
        case WifiProtocol.WPA_WPA2 => {
          wpaConfigure(networkPass, conf);
        }
        case WifiProtocol.WEP => {
          wepConfigure(networkPass, conf);
        }
        case WifiProtocol.OPEN => {
          require(unquotedPass.isEmpty);
          openConfigure(conf);
        }
      }

      wifiManager.addNetwork(finalConfiguration);
    }
    /*}*/
    if (networkId == INVALID_NETWORK_ID) {
      false;
    } else {
      //wifiManager.saveConfiguration();
      wifiManager.disconnect();
      wifiManager.enableNetwork(networkId, true);
      wifiManager.reconnect();
    }
  }

  /*private def eapConfigure(networkPass: String, userName: String, conf: WifiConfiguration): WifiConfiguration = {
    /********************************Configuration Strings****************************************************/
    final String ENTERPRISE_EAP = "TLS";
    final String ENTERPRISE_CLIENT_CERT = "keystore://USRCERT_CertificateName";
    final String ENTERPRISE_PRIV_KEY = "keystore://USRPKEY_CertificateName";
    //CertificateName = Name given to the certificate while installing it

    /*Optional Params- My wireless Doesn't use these*/
    final String ENTERPRISE_PHASE2 = "";
    final String ENTERPRISE_ANON_IDENT = "ABC";
    final String ENTERPRISE_CA_CERT = "";
    /********************************Configuration Strings****************************************************/

    /*Create a WifiConfig*/
    WifiConfiguration selectedConfig = new WifiConfiguration();

    /*AP Name*/
    selectedConfig.SSID = "\"SSID_Name\"";

    /*Priority*/
    selectedConfig.priority = 40;

    /*Enable Hidden SSID*/
    selectedConfig.hiddenSSID = true;

    /*Key Mgmnt*/
    selectedConfig.allowedKeyManagement.clear();
    selectedConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.IEEE8021X);
    selectedConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_EAP);

    /*Group Ciphers*/
    selectedConfig.allowedGroupCiphers.clear();
    selectedConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
    selectedConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
    selectedConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
    selectedConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);

    /*Pairwise ciphers*/
    selectedConfig.allowedPairwiseCiphers.clear();
    selectedConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
    selectedConfig.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);

    /*Protocols*/
    selectedConfig.allowedProtocols.clear();
    selectedConfig.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
    selectedConfig.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

    // Enterprise Settings
    // Reflection magic here too, need access to non-public APIs
    try {
      // Let the magic start
      Class[] wcClasses = WifiConfiguration.class.getClasses();
    // null for overzealous java compiler
    Class wcEnterpriseField = null;

    for (Class wcClass : wcClasses)
      if (wcClass.getName().equals(INT_ENTERPRISEFIELD_NAME))
      {
        wcEnterpriseField = wcClass;
        break;
      }
      boolean noEnterpriseFieldType = false;
      if(wcEnterpriseField == null)
        noEnterpriseFieldType = true; // Cupcake/Donut access enterprise settings directly

      Field wcefAnonymousId = null, wcefCaCert = null, wcefClientCert = null, wcefEap = null, wcefIdentity = null, wcefPassword = null, wcefPhase2 = null, wcefPrivateKey = null;
    Field[] wcefFields = WifiConfiguration.class.getFields();
    // Dispatching Field vars
    for (Field wcefField : wcefFields)
      {
        if (wcefField.getName().equals(INT_ANONYMOUS_IDENTITY))
          wcefAnonymousId = wcefField;
        else if (wcefField.getName().equals(INT_CA_CERT))
          wcefCaCert = wcefField;
        else if (wcefField.getName().equals(INT_CLIENT_CERT))
          wcefClientCert = wcefField;
        else if (wcefField.getName().equals(INT_EAP))
          wcefEap = wcefField;
        else if (wcefField.getName().equals(INT_IDENTITY))
          wcefIdentity = wcefField;
        else if (wcefField.getName().equals(INT_PASSWORD))
          wcefPassword = wcefField;
        else if (wcefField.getName().equals(INT_PHASE2))
          wcefPhase2 = wcefField;
        else if (wcefField.getName().equals(INT_PRIVATE_KEY))
          wcefPrivateKey = wcefField;
      }


      Method wcefSetValue = null;
      if(!noEnterpriseFieldType){
        for(Method m: wcEnterpriseField.getMethods())
        //System.out.println(m.getName());
        if(m.getName().trim().equals("setValue"))
          wcefSetValue = m;
      }


      /*EAP Method*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefEap.get(selectedConfig), ENTERPRISE_EAP);
      }
      else
      {
        wcefEap.set(selectedConfig, ENTERPRISE_EAP);
      }
      /*EAP Phase 2 Authentication*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefPhase2.get(selectedConfig), ENTERPRISE_PHASE2);
      }
      else
      {
        wcefPhase2.set(selectedConfig, ENTERPRISE_PHASE2);
      }
      /*EAP Anonymous Identity*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefAnonymousId.get(selectedConfig), ENTERPRISE_ANON_IDENT);
      }
      else
      {
        wcefAnonymousId.set(selectedConfig, ENTERPRISE_ANON_IDENT);
      }
      /*EAP CA Certificate*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefCaCert.get(selectedConfig), ENTERPRISE_CA_CERT);
      }
      else
      {
        wcefCaCert.set(selectedConfig, ENTERPRISE_CA_CERT);
      }
      /*EAP Private key*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefPrivateKey.get(selectedConfig), ENTERPRISE_PRIV_KEY);
      }
      else
      {
        wcefPrivateKey.set(selectedConfig, ENTERPRISE_PRIV_KEY);
      }
      /*EAP Identity*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefIdentity.get(selectedConfig), userName);
      }
      else
      {
        wcefIdentity.set(selectedConfig, userName);
      }
      /*EAP Password*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefPassword.get(selectedConfig), passString);
      }
      else
      {
        wcefPassword.set(selectedConfig, passString);
      }
      /*EAp Client certificate*/
      if(!noEnterpriseFieldType)
      {
        wcefSetValue.invoke(wcefClientCert.get(selectedConfig), ENTERPRISE_CLIENT_CERT);
      }
      else
      {
        wcefClientCert.set(selectedConfig, ENTERPRISE_CLIENT_CERT);
      }
      // Adhoc for CM6
      // if non-CM6 fails gracefully thanks to nested try-catch

      try{
        Field wcAdhoc = WifiConfiguration.class.getField("adhocSSID");
      Field wcAdhocFreq = WifiConfiguration.class.getField("frequency");
      //wcAdhoc.setBoolean(selectedConfig, prefs.getBoolean(PREF_ADHOC,
      //      false));
      wcAdhoc.setBoolean(selectedConfig, false);
      int freq = 2462;    // default to channel 11
      //int freq = Integer.parseInt(prefs.getString(PREF_ADHOC_FREQUENCY,
      //"2462"));     // default to channel 11
      //System.err.println(freq);
      wcAdhocFreq.setInt(selectedConfig, freq);
      } catch (Exception e)
      {
        e.printStackTrace();
      }

    } catch (Exception e)
    {
      e.printStackTrace();
    }

    WifiManager wifiManag = (WifiManager) getSystemService(Context.WIFI_SERVICE);
    boolean res1 = wifiManag.setWifiEnabled(true);
    int res = wifiManag.addNetwork(selectedConfig);
    Log.d("WifiPreference", "add Network returned " + res );
    boolean b = wifiManag.enableNetwork(selectedConfig.networkId, false);
    Log.d("WifiPreference", "enableNetwork returned " + b );
    boolean c = wifiManag.saveConfiguration();
    Log.d("WifiPreference", "Save configuration returned " + c );
    boolean d = wifiManag.enableNetwork(res, true);
    Log.d("WifiPreference", "enableNetwork returned " + d );
  }*/

  private def openConfigure(conf: WifiConfiguration): WifiConfiguration = {
    conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
    conf;
  }

  /**
   * http://stackoverflow.com/questions/4374862/how-to-programatically-create-and-read-wep-eap-wifi-configurations-in-android/4374934#4374934
   */
  private def wepConfigure(networkPass: String, conf: WifiConfiguration): WifiConfiguration = {
    //WEP IS OLD AND MORE COMPLEX

    //http://developer.android.com/reference/android/net/wifi/WifiConfiguration.Status.html
    //conf.status = WifiConfiguration.Status.DISABLED;

    /**
     * priority: priority group (integer)
      # By default, all networks will get same priority group (0). If some of the
      # networks are more desirable, this field can be used to change the order in
      # which wpa_supplicant goes through the networks when selecting a BSS. The
      # priority groups will be iterated in decreasing priority (i.e., the larger the
      # priority value, the sooner the network is matched against the scan results).
      # Within each priority group, networks will be selected based on security
      # policy, signal strength, etc.
     */
    conf.priority = 40;

    conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);

    conf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
    conf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

    conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
    conf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);

    conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
    conf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);

    conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
    conf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);

    conf.wepKeys.update(0, networkPass);
    conf.wepTxKeyIndex = 0;

    conf;
  }

  /**
   * WPA/WPA2
   */
  private def wpaConfigure(networkPass: String, conf: WifiConfiguration): WifiConfiguration = {
    conf.preSharedKey = networkPass;
    conf;
  }

  private def removeNetworksBySSID(networkSSID: String)
                                  (implicit wifiManager: WifiManager): Unit = {
    val networksWithSSID = wifiManager.getConfiguredNetworks.asScala.filter(network => Option(network.SSID).isDefined);
    val networkIds = networksWithSSID.filter(_.SSID == networkSSID).map(_.networkId);
    networkIds.foreach(wifiManager.removeNetwork);
  }
}
