package com.pligor.myandroid

import android.graphics.{Bitmap, BitmapFactory}
import scala.math
import android.content.res.Resources
import android.content.Context
import android.net.Uri
import java.io.{File, ByteArrayOutputStream, FileNotFoundException, IOException}
import android.provider.MediaStore
import android.graphics.Bitmap.CompressFormat
import android.provider.MediaStore.Images
import scala.util.Random
import com.pligor.myandroid.AndroidFileHelper._
import com.pligor.generic.FileHelper._

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://developer.android.com/training/displaying-bitmaps/load-bitmap.html
 */
object BitmapHelper {
  /**
   * @param subpath the path without the Enviroment.getExternalStorageDirectory part
   * @param imageCompressionQuality range from 1 to 100
   */
  def filePutBitmapToSD(subpath: String,
                        filename: String,
                        bitmap: Bitmap)
                       (implicit
                        compressFormat: CompressFormat,
                        imageCompressionQuality: Option[Int] = None): Unit = {
    //Some formats, like PNG which is lossless, will ignore the quality setting
    writeToSD(subpath, filename,
      bitmap.compress(compressFormat, imageCompressionQuality.getOrElse(100), _)
    );
  }

  def filePutBitmapToTemp(bitmap: Bitmap, ext: String)
                         (implicit
                          compressFormat: CompressFormat,
                          imageCompressionQuality: Option[Int] = None): File = {

    val tempFile = genRandomTemporaryFile(prefix = Some("bitmap"), ext = Some(ext));

    writeToFile(tempFile,
      bitmap.compress(compressFormat, imageCompressionQuality.getOrElse(100), _)
    )

    tempFile
  }

  /**
   * http://stackoverflow.com/questions/8295773/how-can-i-transform-a-bitmap-into-a-uri
   */
  def getUriFromBitmap(bitmap: Bitmap)(implicit context: Context): Option[Uri] = {
    val bytesOutStream = new ByteArrayOutputStream();

    val compressed = bitmap.compress(Bitmap.CompressFormat.PNG, 100, bytesOutStream)

    if (compressed) {
      val name = Random.alphanumeric.take(10).mkString

      val description = null

      Some(
        Uri.parse(
          Images.Media.insertImage(
            context.getContentResolver,
            bitmap,
            name,
            description
          )
        )
      )
    } else None
  }


  def getBitmapFromUri(uri: Uri)(implicit context: Context): Option[Bitmap] = {
    try {
      Some(MediaStore.Images.Media.getBitmap(context.getContentResolver, uri))
    } catch {
      case e@(_: FileNotFoundException | _: IOException) => {
        e.printStackTrace()

        None
      }
    }
  }

  /**
   * Trying to catch the out of memory error would be a BAD idea: http://stackoverflow.com/questions/1692230/is-it-possible-to-catch-out-of-memory-exception-in-java
   */
  def downscaleUri(uri: Uri, targetSize: ImageSizePx)(implicit context: Context): Option[Bitmap] = {
    val options = prepareOptions

    val check = useFileDescriptor(uri) {
      fileDescriptor =>
        assert(
          Option(
            BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options)
          ).isEmpty
        )
    }

    if (check.isDefined) {
      log log ("width before resample: " + options.outWidth)

      log log ("height before resample: " + options.outHeight)
      //we rerun file descriptor segment of code since it can easily not exist (e.g. deleted the 1 msec ago by another thread)

      val bitmapOption =
        useFileDescriptor(uri) {
          fileDescriptor =>
            BitmapFactory.decodeFileDescriptor(
              fileDescriptor,
              null,
              getInSampleSizeOptions(options, targetSize)
            )
        }

      //maybe this is too much because is a downscale of a downscaled bitmap, BUT WITH HAD OTHER ISSUES, WHEN LEFT LOOSE
      bitmapOption.map {
        bitmap =>
          downscaleBitmap(bitmap, targetSize)
      }
    } else {
      None
    }
  }

  /**
   * AVOID USING THIS METHOD. BETTER DEAL WITH BITMAPs IN FILES (URIs).
   * use downscaleUri instead
   */
  private def downscaleBitmap(bitmap: Bitmap, targetSize: ImageSizePx): Bitmap = {
    val currentSize = ImageSizePx(width = Pixel(bitmap.getWidth), height = Pixel(bitmap.getHeight))

    log log "current size: " + currentSize

    val shrinkedSize = currentSize.fitIfLarger(targetSize)

    log log "shrinked size: " + shrinkedSize

    Bitmap.createScaledBitmap(bitmap, shrinkedSize.width.value, shrinkedSize.height.value, true)
  }

  def decodeOptimalSampledBitmapFromFile(pathName: String,
                                         requiredSizeDp: ImageSizeDp)
                                        (implicit context: Context): Bitmap = {
    decodeSampledBitmapFromFile(
      pathName,
      requiredSizeDp.toPx
    )
  }

  def decodeSampledBitmapFromFile(pathName: String,
                                  requiredSizePx: ImageSizePx): Bitmap = {
    val options = prepareOptions
    //the option values are being written
    BitmapFactory.decodeFile(pathName, options) //must return null
    //the option values are being read
    BitmapFactory.decodeFile(
      pathName,
      getInSampleSizeOptions(options, requiredSizePx)
    )
  }

  def decodeOptimalSampledBitmapFromResource(resourceId: Int,
                                             requiredSizeDp: ImageSizeDp)
                                            (implicit context: Context): Bitmap = {
    decodeSampledBitmapFromResource(
      resourceId,
      requiredSizeDp.toPx
    )(context.getResources)
  }

  def decodeSampledBitmapFromResource(resourceId: Int,
                                      requiredSizePx: ImageSizePx)(implicit resources: Resources): Bitmap = {
    val options = prepareOptions
    //the option values are being written
    BitmapFactory.decodeResource(resources, resourceId, options) //must return null
    //the option values are being read
    BitmapFactory.decodeResource(
      resources,
      resourceId,
      getInSampleSizeOptions(options, requiredSizePx)
    )
  }

  private def prepareOptions = {
    val options = new BitmapFactory.Options()
    //Setting the inJustDecodeBounds property to true while decoding avoids memory allocation,
    //returning null for the bitmap object but setting outWidth, outHeight and outMimeType.
    options.inJustDecodeBounds = true

    options
  }

  def getInSampleSizeOptions(options: BitmapFactory.Options,
                             requiredSizePx: ImageSizePx): BitmapFactory.Options = {

    options.inSampleSize = getInSampleSize(
      width = options.outWidth,
      height = options.outHeight,
      requiredWidth = requiredSizePx.width.value,
      requiredHeight = requiredSizePx.height.value
    )

    //Logger("the in sample size is: " + options.inSampleSize);

    options.inJustDecodeBounds = false

    options
  }

  private def getInSampleSize(width: Int, height: Int,
                              requiredWidth: Int, requiredHeight: Int): Int = {
    val finalInSampleSize = if (height > requiredHeight || width > requiredWidth) {

      //Gives sizes which are inside the required dimensions unless they are only a little bit larger
      //Too disproportionate photos get reduced a lot
      //calculate ratios of height and width to requested height and width
      val heightRatio = math.floor(height.toFloat / requiredHeight.toFloat)
      val widthRatio = math.floor(width.toFloat / requiredWidth.toFloat)
      math.max(
        math.max(heightRatio, widthRatio).toInt,
        1 //in case of a zero or something
      )

      /*val halfHeight = height / 2

      val halfWidth = width / 2

      var inSampleSize = 1

      // Calculate the largest inSampleSize value that is a power of 2 and keeps both
      // height and width larger than the requested height and width.
      while ((halfHeight / inSampleSize) > requiredHeight
        //|| //strict, with both sides below target size
        && //loose, with only one side below target size
        (halfWidth / inSampleSize) > requiredWidth) {
        inSampleSize *= 2
      }

      inSampleSize*/
    } else {
      1
    }

    log log "calculated in sample size is: " + finalInSampleSize

    finalInSampleSize
  }

  /**
   * Disadvantage: Does not reduce disproportionate sizes at all !!
   * Advantage: If you get larger by 150% then you are reduced by 2 (200%) in case you really want to shrink it
   */
  /*private def getInSampleSize(width: Int, height: Int,
                      requiredWidth: Int, requiredHeight: Int): Int = {
    if (height > requiredHeight || width > requiredWidth) {
      //calculate ratios of height and width to requested height and width
      val heightRatio = math.round(height.toFloat / requiredHeight.toFloat)

      val widthRatio = math.round(width.toFloat / requiredWidth.toFloat)

      //choose the smallest ratio as inSampleSize value, this means the final image
      //has dimensions close to larger than or equal to the requested height and width
      math.max(
        math.min(heightRatio, widthRatio),
        1 //in case of a zero or something
      )
    } else {
      1
    }
  }*/
}
