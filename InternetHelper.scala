package com.pligor.myandroid

import android.content.{ActivityNotFoundException, Context, Intent}
import android.net.Uri

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
object InternetHelper {
  def showWebSite(url: String)(implicit context: Context): Unit = {
    val webIntent = new Intent(Intent.ACTION_VIEW);
    webIntent.setData(Uri.parse(url));
    try {
      context.startActivity(webIntent);
    }
    catch {
      case e: ActivityNotFoundException => {
        log log "the given url is NOT prepended with http://";
      }
    }
  }

  /**
   * Generally is supported the send to multiple email. Here we support only one
   */
  def requestSendEmail(email: String, subject: String, body: String)
                      (implicit context: Context): Unit = {
    context.startActivity(new Intent(android.content.Intent.ACTION_SEND).
      putExtra(android.content.Intent.EXTRA_EMAIL, Array[String](email)).
      putExtra(android.content.Intent.EXTRA_SUBJECT, subject).
      setType("text/plain").
      putExtra(android.content.Intent.EXTRA_TEXT, body));
  }
}
