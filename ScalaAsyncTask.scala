package com.pligor.myandroid

import com.pligor.generic.FutureHelper._
import scala.concurrent.{Future, ExecutionContext}
import com.pligor.myandroid.AndroidHelper._
import com.pligor.generic.WriteOnce
import android.content.Context

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
abstract class ScalaAsyncTask[Param, Progress, Result] {

  private val cancellorWrittenOnce = new WriteOnce[() => Boolean]()
  
  private val cancelledFlag = new WriteOnce[Unit]()

  def isCancelled = cancelledFlag.isInitialized

  /**
   * @param mayInterruptIfRunning NOT USED YET
   * @return false if typically is already finished
   */
  def cancel(mayInterruptIfRunning: Boolean): Boolean = {
    val willCancel = !cancelledFlag.isInitialized
    
    if(willCancel) {
      cancelledFlag setValue Unit

      if(mayInterruptIfRunning) {
        cancellorWrittenOnce.map(_.apply())
      } else {
        //nop 
      }
    } else {
      //nop
    }

    willCancel
  }

  //TODO make it throw Illegal state exception because you can only call execute ONCE
  def execute(param: Param)
             (implicit context: Context, executionContext: ExecutionContext): Unit = {
    onPreExecute()

    val (f, cancellor) = interruptableFuture({
      future: Future[Boolean] =>
        val result = doInBackground(param)

        onUIwithoutActivity(
          onPostExecute(result)
        )
    })

    cancellorWrittenOnce setValue cancellor
  }

  protected def onPreExecute(): Unit = {
    //nop
  }

  protected def onPostExecute(result: Result): Unit = {
    //nop
  }

  protected def doInBackground(param: Param): Result

  protected def publishProgress(value: Progress)(implicit context: Context): Unit = {
    onUIwithoutActivity(
      onProgressUpdate(value)
    )
  }

  protected def onProgressUpdate(value: Progress): Unit = {
    //nop by default
  }
}

/*class Hello extends AsyncTask[Int, Int, Int] {
  def doInBackground(params: Int*): Int = 0

  override def onProgressUpdate(values: Array[Int]): Unit = {
    publishProgress()
    execute()
    isCancelled

    getStatus
  }

  override def onPostExecute(result: Int)
}*/
