package com.pligor.myandroid

import java.net.URL
import scalaj.http.{HttpOptions, Http}
import play.api.libs.json.JsValue
import scalaj.http.HttpException
import android.content.Context
import com.pligor.myandroid.AndroidHelper._
import com.pligor.generic.{ContentType, HttpHeaderName}

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait JsonRequestBlockingTrait {

  //abstract

  //concrete

  protected case class JsonRequestParam(url: URL,
                                        json: JsValue,
                                        connTimeout_msec: Int,
                                        readTimeout_msec: Int)

  protected case class JsonResponseResult(code: Int, body: String)

  /**
   * @param context THIS IMPLICIT CONTEXT IS VERY CRUCIAL, SCALAJ THROWS EXCEPTION OTHERWISE
   */
  def doJsonRequest[T](jsonRequestParam: JsonRequestParam)
                      (op: Option[JsonResponseResult] => T)(implicit context: Context): T = {
    val jsonResponseOption = {
      try {
        val content = jsonRequestParam.json.toString()

        val request = Http.postData(
          url = jsonRequestParam.url.toString,
          data = content).
          header(HttpHeaderName.CONTENT_TYPE.toString, ContentType.APPLICATION_JSON.toString).
          header(HttpHeaderName.CONTENT_LANGUAGE.toString, getAndroidConfigurationLocale.toString).
          header(HttpHeaderName.CONTENT_LENGTH.toString, content.length.toString).
          option(HttpOptions.connTimeout(jsonRequestParam.connTimeout_msec)).
          option(HttpOptions.readTimeout(jsonRequestParam.readTimeout_msec))

        val (responseCode, headersMap, resultString) = request.asHeadersAndParse(Http.readString)

        Some(JsonResponseResult(responseCode, resultString))
      } catch {
        case e: HttpException => Some(JsonResponseResult(e.code, e.body))
        case e: Exception => None
      }
    }

    op(jsonResponseOption)
  }
}