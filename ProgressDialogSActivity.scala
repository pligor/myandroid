package com.pligor.myandroid

import android.app.{ProgressDialog, Activity}
import scala.collection.mutable
import org.scaloid.common.SActivity

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 */
trait ProgressDialogSActivity extends SActivity {
  private val progressDialogs = mutable.ListBuffer.empty[ProgressDialog]

  def registerProgressDialog(progressDialog: ProgressDialog): ProgressDialog = {
    progressDialogs += progressDialog
    progressDialog
  }

  override def onStop(): Unit = {
    super.onStop()
    progressDialogs foreach {
      progressDialog =>
        if (progressDialog.isShowing) {
          progressDialog.cancel()
        }
    }
  }
}
