package com.pligor.myandroid;

import android.os.AsyncTask;

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * http://developer.android.com/reference/android/os/AsyncTask.html
 * http://blog.nelsonsilva.eu/2009/10/31/scala-on-android-101-proguard-xmlparser-and-function2asynctask
 *
 * remember that according to this: http://stackoverflow.com/questions/9119627/android-sdk-asynctask-doinbackground-not-running-subclass
 * you may NOT have two or more async tasks run simultaneously. in some versions you can up to five but this is not the general occasion
 * probably I will need to use Akka inside android as well
 */
public abstract class JavaAsyncTask<Params, Progress, Result>
		extends AsyncTask<Params, Progress, Result> {

    @Override
	protected final Result doInBackground(Params... params) {
		return doInBackground(params[0]);
	}

	@Override
	protected final void onProgressUpdate(Progress... values) {
		onProgressUpdate(values[0]);    //To change body of overridden methods use File | Settings | File Templates.
	}

	abstract protected Result doInBackground(Params param);

	protected void onProgressUpdate(Progress value) {
		//to be overridden
	}
}
