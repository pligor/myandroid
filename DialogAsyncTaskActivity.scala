package com.pligor.myandroid

import com.pligor.generic.WriteOnce
import android.app.ProgressDialog
import android.content.DialogInterface

/**
 * Created with IntelliJ IDEA.
 * Developer: pligor
 * @deprecated
 */
trait DialogAsyncTaskActivity extends ProgressDialogSActivity {

  protected trait DialogAsyncTask[Params, Progress, Result] extends JavaAsyncTask[Params, Progress, Result] {
    //abstract

    protected def isProgressDialogCancellable: Boolean

    protected def dialogTitleId: Int

    protected def dialogMessageId: Int

    //concrete

    private val progressDialog = new WriteOnce[ProgressDialog]

    override def onPreExecute(): Unit = {
      super.onPreExecute()

      progressDialog setValue {
        val progDialog = registerProgressDialog({
          val indeterminate = true
          val cancelable = isProgressDialogCancellable
          ProgressDialog.show(
            DialogAsyncTaskActivity.this,
            getString(dialogTitleId),
            getString(dialogMessageId),
            indeterminate,
            cancelable
          )
        })

        progDialog.setOnCancelListener(new DialogInterface.OnCancelListener {
          def onCancel(dialog: DialogInterface): Unit = {
            cancel({
              val mayInterruptIfRunning = true
              mayInterruptIfRunning
            })
          }
        })

        progDialog
      }
    }

    override def onPostExecute(result: Result): Unit = {
      super.onPostExecute(result)

      assert(progressDialog.isInitialized)
      progressDialog().dismiss()
    }
  }

}
