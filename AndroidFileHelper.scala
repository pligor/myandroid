package com.pligor.myandroid

import java.io._
import android.os.Environment
import java.util.zip.{ZipEntry, ZipOutputStream}
import android.content.Context
import android.net.Uri
import scala.io.Source
import org.apache.commons.codec.binary.Base64InputStream
import com.pligor.generic.FileHelper

/**
  * Created with IntelliJ IDEA.
  * User: pligor
  */
object AndroidFileHelper {
   val base64overhead = +33.0 / 100.0;

  /**
   * write in temporary directory only if it is located inside sd card, otherwise use the root of the sd card as temp
   * @return
   */
  def getTempDir: File = {
    val tmpdir = FileHelper.getTempDir

    val externalDir = Environment.getExternalStorageDirectory

    val chosenDir = if (isInsideDir(tmpdir, externalDir)) {
      tmpdir
    } else {
      externalDir
    }

    require(chosenDir.isDirectory)

    chosenDir
  }

   //implicits
   implicit def func2filenameFilter(func: (File, String) => Boolean) = new FilenameFilter {
     def accept(dir: File, name: String) = func(dir, name);
   }

   /**
    * @param fileOrDir file or directory to test
    * @param dir the directory which must be inside in order to return true, otherwise false
    */
   def isInsideDir(fileOrDir: File, dir: File): Boolean = {
     //fileOrDir.getCanonicalPath
     require(dir.isDirectory);
     val dirPath = dir.getCanonicalPath;
     fileOrDir.getCanonicalPath.take(dirPath.length) == dirPath;
   }

   def getFileContents(filepathOrUrl: String): String = {
     val file = new File(filepathOrUrl);
     if (file.exists()) {
       Source.fromFile(file).mkString;
     }
     else {
       Source.fromURL(filepathOrUrl).mkString;
     }
   }

   def useFileDescriptor[T](uri: Uri)(op: (FileDescriptor) => T)(implicit context: Context): Option[T] = {
     try {
       val assetFileDescriptorOption = Option(context.getContentResolver.openAssetFileDescriptor(uri, "r"));
       if (assetFileDescriptorOption.isDefined) {
         val assetFileDescriptor = assetFileDescriptorOption.get;
         try {
           val fileDescriptorOption = Option(assetFileDescriptor.getFileDescriptor);
           if (fileDescriptorOption.isDefined) {
             Some(op(fileDescriptorOption.get));
           } else {
             None;
           }
         } finally {
           assetFileDescriptor.close();
         }
       } else {
         None;
       }
     }
     catch {
       case e@(_: FileNotFoundException | _: IOException) => {
         e.printStackTrace();
         None;
       }
     }
   }

   def fileExists(fullpath: String): Boolean = new File(fullpath).exists;

   def fileDelete(fullpath: String): Boolean = new File(fullpath).delete();

   def clearDirSafe(dir: File): Option[Boolean] = {
     if(dir.exists()) {
       Some(clearDir(dir));
     } else {
       None;
     }
   }

   def clearDir(dir: File) = {
     require(dir.isDirectory, "you are trying to clear the contents of a File that is not a directory");
     dir.listFiles().forall(_.delete());
   }

   def sdSubpath2fullpath(subpath: String) = {
     Environment.getExternalStorageDirectory + File.separator + subpath
   }

   /**
    * recursive
    */
   private def mkdir_to_sd(subpath: String): Boolean = {
     new File(sdSubpath2fullpath(subpath)).mkdirs();
   }

   /**
    * TESTED OK
    */
   private def put_trailing_slash(path: String): String = {
     if (path.substring(path.length - 1).contentEquals("/")) {
       return path
     }
     path + "/";
   }

   def file_put_contents_to_sd(subpath: String, filename: String, data: String) {
     writeToSD(subpath, filename, _.write(data.getBytes));
   }

   /**
    * remember that apps do not have the permission to write to any file
    * they can only write to the sd card (given the permission of course)
    */
   def writeToFile(file: File, writeToFileOutputStream: FileOutputStream => Unit): Unit = {
     val fout: FileOutputStream = new FileOutputStream(file)

     try {
       writeToFileOutputStream(fout)

       fout.flush()
     } catch {
       case e: IOException => {
         e.printStackTrace()

         log log "io exception ??"
       }
     } finally {
       fout.close()
     }
   }

   def writeToSD(subpath: String,
                         filename: String,
                         writeToFileOutputStream: (FileOutputStream) => Unit): Unit = {
     mkdir_to_sd(subpath)

     val file: File = new File(sdSubpath2fullpath(subpath), filename)

     writeToFile(file, writeToFileOutputStream)
   }

   /**
    * http://stackoverflow.com/questions/7598135/how-to-read-a-file-as-a-byte-array-in-scala
    * @param file some file (not folder)
    * @return
    */
   def fileToByteArray(file: File): Array[Byte] = {
     require(file.isFile);
     val bis = new BufferedInputStream(new FileInputStream(file));
     Stream.continually(bis.read).takeWhile(_ != -1).map(_.toByte).toArray;
   }

   def areFilesEqual(fileLeft: File, fileRight: File): Boolean = {
     require(fileLeft.isFile && fileRight.isFile);

     fileToByteArray(fileLeft).toList == fileToByteArray(fileRight).toList;
   }

   /**
    * @param doEncode on false decode
    */
   def useEachBase64Buffer(inputStream: InputStream, bufferBytesLen: Int, doEncode: Boolean)(bufferExploit: (Array[Byte]) => _) {
     //val b64InputStream = new Base64InputStream(fis, Base64.DEFAULT);
     val b64InputStream = new Base64InputStream(
       inputStream,
       doEncode,
       0, //line length is zero because we want everything in a single line
       null //line seperator is not used if line length is zero
     );
     try {
       FileHelper.useEachBuffer(b64InputStream, bufferBytesLen)(bufferExploit)
     } finally {
       b64InputStream.close();
     }
   }

   /**
    * http://stackoverflow.com/questions/4604237/how-to-write-to-a-file-in-scala
    * @param f file
    * @param op operation
    * @return
    */
   def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
     val p = new java.io.PrintWriter(f)
     try {
       op(p)
     } finally {
       p.close()
     }
   }

   def compress(zipFilepath: String, files: List[File]) {
     def readByte(bufferedReader: BufferedReader): Stream[Int] = {
       bufferedReader.read() #:: readByte(bufferedReader);
     }
     val zip = new ZipOutputStream(new FileOutputStream(zipFilepath));
     try {
       for (file <- files) {
         //add zip entry to output stream
         zip.putNextEntry(new ZipEntry(file.getName));

         val in = Source.fromFile(file.getCanonicalPath).bufferedReader();
         try {
           readByte(in).takeWhile(_ > -1).toList.foreach(zip.write(_));
         }
         finally {
           in.close();
         }

         zip.closeEntry();
       }
     }
     finally {
       zip.close();
     }
   }
 }
